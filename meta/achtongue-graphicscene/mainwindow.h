#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDesktopWidget>
#include <QKeyEvent>
#include <QDebug>
#include <QTimer>
#include "engine/player.h"
#include "engine/bonusmanager.h"
#include "graphics/gamescene.h"
#include "graphics/bonusitem.h"
#include "engine/player.h"
#include "engine/localplayer.h"
#include "engine/followplayer.h"
#include "engine/net.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Net net;

protected:
    bool event(QEvent *event);

private:
    Ui::MainWindow *ui;

//private slots:
//    event();
};

#endif // MAINWINDOW_H
