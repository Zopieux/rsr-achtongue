#include "mainwindow.h"
#include "const.h"
#include "ui_mainwindow.h"
#include "engine/engine.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Engine *engine = Engine::instance();

    ui->graphicsView->setScene(engine->gameScene());
    ui->graphicsView->setMinimumSize(ui->graphicsView->sceneRect().toRect().size());
    connect(ui->graphicsView, SIGNAL(viewChanged()), engine->gameScene(), SLOT(updateFrame()));
    engine->init();

    QVector<QColor> officialColors;
    officialColors << QColor(255, 233, 43) << QColor(2, 209, 192) << QColor(255, 164, 186)
                   << QColor(68, 68, 255) << QColor(106, 255, 48) << QColor(255, 136, 52)
                   << QColor(255, 69, 69) << QColor(255, 255, 255);

    // TODO: remove this manual player creation, replace with "register on server"
    // registerOnServer("Dupont")
    // wait for ACK from server with my position, direction and color
    net.registerOnServer("Rivers");
    QByteArray data = net.data;
    if(!protocol::isRoomFull(data)){
        int id = protocol::getIdFromParams(data);
        int port = protocol::getSndPortFromParams(data);
        net.joinRoom(id, port);
        QColor color = protocol::getColor(data);
//        QObject::connect(net, SIGNAL(connectionTimeout()), this, SLOT(quit()));
        Player *player;
        if (true) {
            player = new LocalPlayer("Rivers",
                                     PlayerColors() << color,
                                     Qt::Key_Left, Qt::Key_Right);
            if (false) {
                player->addTrace();
                player->traceAt(1)->offsetDirection(M_PI / 5);
                player->addTrace();
                player->traceAt(2)->offsetDirection(-M_PI / 5);
            }
            engine->addPlayer(player);
        }
    } else {
         qDebug()<< "Main :: room Full";
    }
//    if (true) {
//        player = new LocalPlayer("Forest",
//                                 PlayerColors() << officialColors.at(1),
//                                 Qt::Key_Q, Qt::Key_D);
//        engine->addPlayer(player);
//    }
//    if (true) {
//        player = new LocalPlayer("Anaconda",
//                                 PlayerColors() << officialColors.at(2) << officialColors.at(4),
//                                 Qt::Key_H, Qt::Key_K);
//        engine->addPlayer(player);
//    }
//    if (false) {
//        player = new FollowPlayer("R2D2", PlayerColors() << officialColors.at(2), engine->playerAt(0));
//        engine->addPlayer(player);
//    }

    QDesktopWidget *desktop = QApplication::desktop();
    const QRect &s = desktop->screenGeometry(desktop->primaryScreen());
    move(s.width() / 2, s.height() / 2);
    setFocus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::event(QEvent *rawEvent)
{
    if (rawEvent->type() == QEvent::KeyPress || rawEvent->type() == QEvent::KeyRelease) {
        bool pressed = rawEvent->type() == QEvent::KeyPress;
        QKeyEvent *event = static_cast<QKeyEvent *>(rawEvent);
        if (event->isAutoRepeat())
            return false;
        if (event->key() == Qt::Key_Space && Engine::instance()->phase() == Engine::LOBBY) {
            Engine::instance()->startRound();
            return true;
        }
        return Engine::instance()->keyEvent(Qt::Key(event->key()), pressed);
    }
    return QMainWindow::event(rawEvent);
}
