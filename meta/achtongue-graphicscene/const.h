#ifndef CONST_H
#define CONST_H

#include <QSize>
#include <QRect>
#include <QVector>
#include <QColor>

static const QSize BONUS_SIZE(43, 43);
static const QSize SCENE_SIZE(700, 700);
static const int BORDER_WIDTH = 4;
static const QColor BACKGROUND_COLOR = QColor(30, 30, 30);
typedef QVector<QColor> PlayerColors;
typedef enum { STRAIGHT, LEFT, RIGHT} PlayerMove;
typedef qint64 tick_t;

Q_DECLARE_METATYPE(PlayerMove)

#endif // CONST_H
