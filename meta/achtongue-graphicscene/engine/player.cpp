#include "engine/player.h"
#include <QDebug>

Player::Player(const QString &name, const PlayerColors &colors) :
    mName(name), mColors(colors), mAlive(true)
{
}

Player::~Player()
{
    while (!mTraces.empty())
        delete mTraces.takeLast();
}

bool Player::alive()
{
    return mAlive;
}

void Player::addTrace()
{
    PlayerTrace *trace = new PlayerTrace(traceAt(0));
    mTraces.append(trace);
}

void Player::init()
{
    while (!mTraces.empty())
        delete mTraces.takeLast();
    PlayerTrace *trace = new PlayerTrace(mColors, this);
    mTraces.append(trace);
    mAlive = true;
}

void Player::setTracing(bool tracing)
{
    foreach (PlayerTrace *trace, mTraces) trace->setTracing(tracing);
}
void Player::setPos(const QPointF &pos)
{
    foreach (PlayerTrace *trace, mTraces) trace->setPos(pos);
}

void Player::setWidth(qreal width)
{
    foreach (PlayerTrace *trace, mTraces) trace->setWidth(width);
}
void Player::setDirection(qreal direction)
{
    foreach (PlayerTrace *trace, mTraces) trace->setDirection(direction);
}
void Player::setSpeed(qreal speed)
{
    foreach (PlayerTrace *trace, mTraces) trace->setSpeed(speed);
}
void Player::setTurnRadius(qreal turnRadius)
{
    foreach (PlayerTrace *trace, mTraces) trace->setTurnRadius(turnRadius);
}
void Player::setColors(const PlayerColors &colors)
{
    mColors = colors;
    foreach (PlayerTrace *trace, mTraces) trace->setColors(colors);
}
void Player::setMove(PlayerMove move)
{
    foreach (PlayerTrace *trace, mTraces) trace->setMove(move);
}
void Player::setMoving(bool moving)
{
    foreach (PlayerTrace *trace, mTraces) trace->setMoving(moving);
}
void Player::clearTail()
{
    foreach (PlayerTrace *trace, mTraces) trace->clearTail();
}
void Player::update(qint64 tick)
{
    foreach (PlayerTrace *trace, mTraces) trace->update(tick);
}
