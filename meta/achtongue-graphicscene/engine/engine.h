#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QTimer>
#include "const.h"
#include "engine/bonusmanager.h"
#include "engine/player.h"
#include "graphics/gamescene.h"
#include "engine/net.h"

class Engine : public QObject
{
    Q_OBJECT
public:
    static qreal TICK_RATE;
    static qreal DEFAULT_BONUS_SPAWN_PROBA;
    static tick_t PREROUND_DURATION;

    enum Phase {
        LOBBY, MATCHMAKING, PREROUND, ROUND, ROUNDBREAK
    };

    explicit Engine(QObject *parent = 0);
    ~Engine();

    static Engine *instance(bool createIfNecessary = true);

    void init();
    inline GameScene *gameScene() const
    {
        return mGameScene;
    }
    inline QList<Player *> players() const
    {
        return mPlayers;
    }
    inline Phase phase() const
    {
        return mPhase;
    }
    inline Player *playerAt(int id) const
    {
        return mPlayers.at(id);
    }
    QSet<Bonus *> bonusForPlayer(PlayerTrace *trace) const;

    inline QPointF randomPos() const;

signals:
    void beforeGameTick(tick_t tick);
    void afterGameTick(tick_t tick);

public slots:
    bool keyEvent(Qt::Key key, bool pressed);
    void addPlayer(Player *player);
    void startRound();
    void endRound();

private slots:
    void gameTick();

private:
    bool mInitialized;
    bool mRequireFullUpdate;
    Phase mPhase;
    tick_t mTick;
    qreal mBonusSpawnProba;
//    int roundNumber;

    GameScene *mGameScene;
    BonusManager *mBonusManager;
    QList<Player *> mPlayers;
    QTimer *mUpdateTimer;

    static Engine *sInstance;
};

#endif // ENGINE_H
