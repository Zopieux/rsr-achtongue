#include "net.h"

Net::Net(QObject *parent, QString server, int port) :
    QObject(parent), server(server), port(port)
{
    QThread* t = new QThread();
//    connect(this, SIGNAL(readyToEnterTheRoom(int,int)), this, SLOT(do_setup(int,int)));
//    connect(t,SIGNAL(started()),this,SLOT(run()));
    this->moveToThread(t);
    t->start();
}

void Net::registerOnServer(QString name)
{
    QTcpSocket s;
    qDebug() <<"Net::trying to contact server:" << server <<":"<<port;
    s.connectToHost(server,port);
    if(s.waitForConnected(1000))
    {
        qDebug("Net::Connected.");
        s.write(protocol::helloServer(name)); // send username
        s.waitForBytesWritten(1000);
        s.waitForReadyRead(3000);
        data = s.readAll();
        s.disconnectFromHost();
    }
    else
    {
        qDebug("Net::Timeout.. Could not connect to game Server");
    }
}

void Net::joinRoom(int id, int port){
    qDebug()<<"Net::readyToEnterRoom emitted";
    doSetup(id, port);
    socket = new QTcpSocket();
    socket->connectToHost(server, port);
    qDebug()<<"Net::connected to room";
    if(socket->waitForConnected(10000))
    {
        socket->write(protocol::joinRoom(id));
        qDebug("Net::joinRoom sent");
        socket->waitForBytesWritten(1000);
        qDebug() <<"Net:: waiting for the game to start";
        if(socket->waitForReadyRead())
        {
            data = socket->readAll();
            if(protocol::isGameStart(data))
            {
                qDebug() <<"Net:: game Started";

                // emit signal to inteface for countDown
                connect(socket,SIGNAL(readyRead()),this,SLOT(readData()));
            }
        }
        else
        {
            qDebug()<<"Net::Timout.. Didn't receive game start in time";
            emit connectionTimeout();
        }
    }
    else
    {
        qDebug("Net::Timeout.. Could not connect to game Room");
        emit connectionTimeout();
    }
}

//need to be checked
QByteArray Net::fetchCommand()
{
    qDebug()<<"Net::fetchCommand";
    QByteArray elt;
    mutex.lock();
    elt = queue.dequeue();
    mutex.unlock();
    return elt;
}


void Net::sendAction(int value,int xPos,int yPos)
{
    qDebug()<<"Net::sendAction";
    socket->write(protocol::action(value,xPos,yPos,playerId));
    socket->waitForBytesWritten(1000);
}

void Net::readData()
{
    mutex.lock();
    QByteArray cmd = socket->readAll();
    queue.append(cmd);
    mutex.unlock();
}

bool Net::newCommand()
{
    qDebug()<<"Net::newCommand";
    bool ok = true;
    mutex.lock();
    if (queue.isEmpty()){
        ok = false;
    }
    mutex.unlock();
    return ok;
}

void Net::doSetup (int id, int p)
{
    qDebug()<<"Net::do_setup";
    mutex.lock();
    playerId = id;
    roomPort = p;
    isReady = true;
    mutex.unlock();
    qDebug() << "Net::do_setup->playerId = "<<playerId;
    qDebug() << "Net::do_setup->roomPort = "<<roomPort;
}

int Net::getRemoteID()
{
    int n;
    mutex.lock();
    if (isReady)
    {
        n = playerId;
    }
    else
    {
        n = -1;
    }
    mutex.unlock();
    return n;
}

void Net::sendData(QByteArray data)
{
    mutex.lock();
    socket->write(data);
    socket->waitForBytesWritten(3000);
    mutex.unlock();
}
