#ifndef BONUS_H
#define BONUS_H

#include <functional>
#include <QtGlobal>
#include <QDebug>
#include <QList>
#include "engine/player.h"

struct BonusType {
    int uid;  // 0 if stackable, > 0 for unique ID, < 0 for global effect
    tick_t duration;
    enum Affects {
        SELF, OTHERS, ALL
    } affects;
    QString icon;
    std::function<void()> prologue;
    std::function<void()> update;
    std::function<void()> epilogue;
    std::function<void(PlayerTrace*)> prologueTrace;
    std::function<void(PlayerTrace*)> updateTrace;
    std::function<void(PlayerTrace*)> epilogueTrace;
    inline bool instant() const { return duration == 0; }
    inline bool stackable() const { return uid == 0; }
};

class Bonus
{
public:
    static QList<BonusType> BonusTypes;

    explicit Bonus(const BonusType &type, PlayerTrace *trace = 0);
    virtual ~Bonus() {}
    inline int typeUid() const {
        return mType.uid;
    }
    inline bool global() const {
        return mType.uid < 0;
    }
    inline tick_t duration() const {
        return mType.duration;
    }
    inline tick_t tick() const
    {
        return mTick;
    }
    inline tick_t remaining() const
    {
        tick_t diff = mType.duration - mTick;
        Q_ASSERT(diff >= 0);
        return diff;
    }
    inline bool done() const
    {
        return mDone;
    }
    inline qreal completion() const
    {
        return mType.duration != 0 ? (qreal)remaining() / mType.duration : 1.;
    }
    void reset();
    void tick();
    void tickGlobal();

private:
    Bonus(const Bonus&) = delete;
    inline bool lastFrame() const
    {
        return remaining() == 0;
    }
    BonusType mType;
    PlayerTrace *mTrace;
    tick_t mTick;
    bool mDone;
};
#endif
