#include "protocol.h"

protocol::protocol()
{       
}

int protocol::messageLength = 6;

QByteArray protocol::action(int MOVE, int xPos, int yPos, int playerID)
{
    int data[messageLength];
    data[0] = 0; // type action:0
    data[1] = 4;  // length of the content of the packet
    data[2] = playerID;
    data[3] = MOVE;
    data[4] = xPos;
    data[5] = yPos;
    for(int i=6; i < messageLength;i++)
    {
        data[i]= 0;
    }

    QString s = "";
    for(int i=0; i<messageLength;i++)
    {
        if (i>1)
        {
            s+= ".";
        }
        s+= QString("%s").arg(data[i]);
    }
    return s.toUtf8();
}

int protocol::getMessageLength()
{
    return messageLength;
}

QByteArray protocol::bonus(int type,int xPos, int yPos)
{
    int data[messageLength];
    data[0] = 1; // type bonus:0
    data[1] = 3;  // length of the content of the packet
    data[2] = type;
    data[3] = xPos;
    data[4] = yPos;
    for (int i=5; i < messageLength; i++ )
    {
        data[i] = 0;
    }
    QString s = "";
    for(int i=0; i<messageLength;i++)
    {
        if (i>1)
        {
            s+= ".";
        }
        s+= QString("%s").arg(data[i]);
    }
    return s.toUtf8();
}
QByteArray protocol::gameParams(int id,int sndp, qreal startx, qreal starty, QColor color)
{
    QString s = "3.";
    s += QString::number(id);
    s += ".";
    s += QString::number(sndp);
    s += ".";
    s += QString::number(startx);
    s += ".";
    s += QString::number(starty);
    s += ".";
    s += QString::number(color.red());
    s += ".";
    s += QString::number(color.green());
    s += ".";
    s += QString::number(color.blue());
    return s.toUtf8();
}

QColor protocol::getColor(QByteArray data){
    QStringList slist = ((QString) data).split(".");
    int type = slist.at(0).toInt();
    return (type == 3) ? QColor::fromRgb(slist.at(4).toInt(), slist.at(5).toInt(), slist.at(6).toInt()) : QColor();
}

QByteArray protocol::helloServer(QString username)
{
    QString s = "2.";
    s += username;
    return s.toUtf8();
}

QByteArray protocol::joinRoom(int playerId)
{
    QString s = "4.";
    s += QString::number(playerId);
    return s.toUtf8();
}

QByteArray protocol::gameStart(int xPos, int yPos, qreal direction)
{
    QString s = "5.";
    s += QString::number(xPos);
    s += ".";
    s += QString::number(yPos);
    s += ".";
    s += QString::number(direction);
    return s.toUtf8();
}

QByteArray protocol::roomFull()
{
    QString s = "7.";
    s += "0";
    return s.toUtf8();
}

// message usage

int  protocol::getIdFromParams(QByteArray data)
{
   QStringList slist = ((QString) data).split(".");
   int type = slist.at(0).toInt();
   if (type == 3)
   {
        QString s = slist.at(1);
        return s.toInt();
   }
   else
   {
       return -1;
   }
}

int  protocol::getSndPortFromParams(QByteArray data)
{
   QStringList slist = ((QString) data).split(".");
   int type = ((QString)slist.at(0)).toInt();
   if (type == 3)
   {
        QString s = slist.at(2);
        return s.toInt();
   }
   else
   {
       return -1;
   }
}

int  protocol::getIdFromJoinRoom(QByteArray data)
{
   QStringList slist = ((QString) data).split(".");
   int type = ((QString)slist.at(0)).toInt();
   if (type == 4)
   {
        QString s = slist.at(1);
        return s.toInt();
   }
   else
   {
       return -1;
   }
}

QString protocol::getUsernameFromHelloServer(QByteArray data)
{
    QStringList slist = ((QString) data).split(".");
    int type = ((QString)slist.at(0)).toInt();
    if (type == 2)
    {
         QString s = slist.at(1);
         return s;
    }
    else
    {
        return "";
    }
}

qreal getDirectionFromStartGame(QByteArray data )
{
    QStringList slist = ((QString) data).split(".");
    int type = ((QString)slist.at(0)).toInt();
    if (type == 5)
    {
         QString s = slist.at(3);
         if (slist.length()> 3)
         {
             s += ".";
             s+= slist.at(4);
         }
         try
         {
            return s.toDouble();
         }
         catch(int e)
         {
             return s.toFloat();
         }
    }
    else
    {
        return -1;
    }
}

QPoint getPositionFromStartGame(QByteArray data)
{

    QStringList slist = ((QString) data).split(".");
    int type = ((QString)slist.at(0)).toInt();
    if (type == 5)
    {
         int x = slist.at(1).toInt();
         int y = slist.at(2).toInt();
         QPoint p(x,y);
         return p;
    }
    else
    {
        QPoint p(0,0);
        return p;
    }
}


// message type

bool protocol::isAction(QByteArray data)
{
    return check(data,0);
}

bool protocol::isBonus(QByteArray data)
{
    return check(data,1);
}

bool protocol::isHelloServer(QByteArray data)
{
    return check(data,2);
}

bool protocol::isGameParams(QByteArray data)
{
    return check(data,3);
}

bool protocol::isJoinRoom(QByteArray data)
{
    return check(data,4);
}

bool protocol::isGameStart(QByteArray data)
{
    return check(data,5);
}

bool protocol::isGameOver(QByteArray data)
{
    return check(data,6);
}

bool protocol::isRoomFull(QByteArray data)
{
    return check(data,7);
}


bool protocol::check(QByteArray data,int typeToCheck)
{
    QStringList slist = ((QString) data).split(".");
    int type = ((QString)slist.at(0)).toInt();
    if (type == typeToCheck)
    {
        return true;
    }
    else
    {
        return false;
    }
}
