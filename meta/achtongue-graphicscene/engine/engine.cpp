#include "engine/engine.h"
#include "engine/localplayer.h"
#include "utils.h"

Engine *Engine::sInstance = NULL;
qreal Engine::TICK_RATE = 1.f / 40;
qreal Engine::DEFAULT_BONUS_SPAWN_PROBA = 1.f / 50;
tick_t Engine::PREROUND_DURATION = 40;


Engine::Engine(QObject *parent) :
    QObject(parent), mInitialized(false), mRequireFullUpdate(false), mPhase(LOBBY), mTick(0),
    mBonusSpawnProba(DEFAULT_BONUS_SPAWN_PROBA)
{
    mUpdateTimer = new QTimer(this);
    mUpdateTimer->setTimerType(Qt::PreciseTimer);
    mUpdateTimer->setInterval(TICK_RATE * 1000);
    mUpdateTimer->setSingleShot(false);
    mGameScene = new GameScene(this);
    mBonusManager = new BonusManager(this);

    connect(mUpdateTimer, SIGNAL(timeout()), this, SLOT(gameTick()));
}

Engine::~Engine()
{
    delete mUpdateTimer;
    delete mGameScene;
    delete mBonusManager;
}

Engine *Engine::instance(bool createIfNecessary)
{
    if (!sInstance && createIfNecessary) {
        qDebug() << "ENGINE INSTANCE CREATED!";
        sInstance = new Engine;
    }
    return sInstance;
}

void Engine::init()
{
    if (mInitialized)
        return;
    mInitialized = true;

    mGameScene->init();

}

QSet<Bonus *> Engine::bonusForPlayer(PlayerTrace *trace) const
{
    return mBonusManager->bonusForPlayer(trace);
}

QPointF Engine::randomPos() const
{
    static const int m = 40;
    const QRectF &r = gameScene()->sceneRect();
    return QPointF(Utils::qrandRange(m, r.width() - m), Utils::qrandRange(m, r.height() - m));
}

bool Engine::keyEvent(Qt::Key key, bool pressed)
{
    bool handled = false;
    foreach (Player *player, mPlayers) {
        if (key == Qt::Key_Space && pressed) {
            player->clearTail();
            mRequireFullUpdate = true;
            handled = true;
        }
        if (key == Qt::Key_Enter && pressed) {
            player->setTracing(!pressed);
            handled = true;
        }
        LocalPlayer *lplayer = dynamic_cast<LocalPlayer *>(player);
        if (lplayer == NULL)
            continue;
        if (lplayer->handleKey(key, pressed))
            handled = true;
    }
    return handled;
}

void Engine::addPlayer(Player *player)
{
    mPlayers.append(player);
}

void Engine::startRound()
{
    // TODO: do all that from server
    qDebug() << "start round";
    mPhase = PREROUND;
    mTick = 0;
    // Place player randomly
    foreach (Player *player, mPlayers) {
        player->init();
        player->setDirection((qrand() % 1000) * M_PI * 2 / 1000);
        player->setMoving(true);
        player->setPos(randomPos());
    }
    mUpdateTimer->start();
}

void Engine::endRound()
{
    mUpdateTimer->stop();
}

void Engine::gameTick()
{
    // Called at each frame
    // TODO: unstack from network order stack and apply
    emit beforeGameTick(mTick);
    if (mTick == PREROUND_DURATION) {
        mPhase = ROUND;
        foreach (Player *player, mPlayers) {
            player->setTracing(true);
        }
    }
    // TODO: this must come from server
    if (mTick % 4 == 0) {
        if (mPhase == ROUND && (qreal)qrand() / RAND_MAX < mBonusSpawnProba) {
            mBonusManager->spawnRandomBonus();
        }
    }
    mBonusManager->update(mTick);
    foreach (Player *player, mPlayers) {
        player->update(mTick);
    }
    if (mRequireFullUpdate) {
        mRequireFullUpdate = false;
        mGameScene->update();
    }
    mTick++;
    emit afterGameTick(mTick);
}
