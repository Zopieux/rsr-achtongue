#include "engine/followplayer.h"
#include <QPointF>
#include <QDebug>
#include <QtMath>

FollowPlayer::FollowPlayer(const QString &name, const PlayerColors &colors,
                           Player *playerFollowing) :
    QObject(), Player(name, colors), following(playerFollowing->traceAt(0)), random(1.f)
{
    actTimer = new QTimer(this);
    actTimer->setInterval(150);
    actTimer->setSingleShot(false);
    connect(actTimer, SIGNAL(timeout()), this, SLOT(follow()));
    actTimer->start();
}

FollowPlayer::~FollowPlayer()
{
    actTimer->stop();
    delete actTimer;
}

void FollowPlayer::follow()
{
    if (!following->alive()) {
        actTimer->stop();
        return;
    }
    qreal angle = qAtan2(following->pos().y() - traceAt(0)->pos().y(),
                         following->pos().x() - traceAt(0)->pos().x());
    qreal delta = fmod(traceAt(0)->state().direction - angle, M_PI * 2 - 0.1);
    if (qAbs(delta) < M_PI / 16)
        setMove(STRAIGHT);
    else
        setMove(delta > 0 ? LEFT : RIGHT);
}
