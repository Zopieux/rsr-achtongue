#ifndef BONUSMANAGER_H
#define BONUSMANAGER_H

#include <QObject>
#include <QMap>
#include <QSet>
#include "engine/bonus.h"
#include "graphics/bonusitem.h"
#include "graphics/gamescene.h"
#include "engine/player.h"

class BonusManager : public QObject
{
    Q_OBJECT
public:
    explicit BonusManager(QObject *parent = 0);
    ~BonusManager();
    QSet<Bonus *> bonusForPlayer(PlayerTrace *trace) const;

signals:
public slots:
    void update(qint64 tick);
    BonusItem *spawnRandomBonus();

private:
    void updatePlayer(Player *player, tick_t tick);
    void updatePlayerTrace(PlayerTrace *playerTrace, tick_t tick);
    void playerBonusPick(PlayerTrace *trace, BonusItem *bonusItem);
    Bonus *playerHasEffect(PlayerTrace *trace, const BonusType &type) const;
    Bonus *globalHasEffect(const BonusType &type) const;

    QSet<Bonus *> mGlobalBonus;
    QMap<Bonus *, QSet<PlayerTrace *> > mBonusToPlayers;
    QMap<PlayerTrace *, QSet<Bonus *> > mPlayerToBonus;
};

#endif // BONUSMANAGER_H
