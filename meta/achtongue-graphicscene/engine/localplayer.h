#ifndef LOCALPLAYER_H
#define LOCALPLAYER_H

#include <QtCore>
#include <QVector>
#include <QByteArray>
#include "player.h"
#include "protocol.h"

class LocalPlayer : public Player
{
public:
    explicit LocalPlayer(const QString &name, const PlayerColors &colors,
                         Qt::Key mLeftKey, Qt::Key mRightKey);

    Qt::Key leftKey() const;
    Qt::Key rightKey() const;
    void setLeftKey(const Qt::Key &value);
    void setRightKey(const Qt::Key &value);
    bool handleKey(Qt::Key key, bool pressed);

signals:
    void sendAction(int);

private:
    Qt::Key mLeftKey, mRightKey;
    QVector<Qt::Key> mKeyStack;

};

#endif // LOCALPLAYER_H
