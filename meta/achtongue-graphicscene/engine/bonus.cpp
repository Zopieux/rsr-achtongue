#include "engine/bonus.h"
#include "engine/engine.h"
#include <QDebug>

Bonus::Bonus(const BonusType &type, PlayerTrace *trace) :
    mType(type), mTrace(trace), mTick(0), mDone(false)
{
}

void Bonus::reset()
{
    if (done())
        return;
    // FIXME: 0 would be better but it would call prologue() once again! we need a flag prologueDone here.
    mTick = 1;
}

void Bonus::tick()
{
    if (done())
        return;

    if (mTick == 0) {
        // first time!
        if (mType.prologueTrace && mTrace) mType.prologueTrace(mTrace);
    }

    if (lastFrame()) {
        Q_ASSERT(!done());
        mDone = true;
        if (mType.epilogueTrace && mTrace) mType.epilogueTrace(mTrace);
        return;
    }
    if (mType.updateTrace && mTrace) mType.updateTrace(mTrace);
    mTick++;
}

void Bonus::tickGlobal()
{
    if (done())
        return;

    if (mTick == 0) {
        // first time!
        if (mType.prologue) mType.prologue();
    }

    if (lastFrame()) {
        Q_ASSERT(!done());
        mDone = true;
        if (mType.epilogue) mType.epilogue();
        return;
    }
    if (mType.update) mType.update();
    mTick++;
}

QList<BonusType> Bonus::BonusTypes = QList<BonusType>()
        // self; slower
        << BonusType {
           .uid = 0, .duration = 300, .affects = BonusType::SELF, .icon = "Sl",
           .prologue = NULL, .update = NULL, .epilogue = NULL,
           .prologueTrace = [](PlayerTrace *trace) { trace->setSpeed(trace->speed() / 2.f); },
           .updateTrace = NULL,
           .epilogueTrace = [](PlayerTrace *trace) { trace->setSpeed(trace->speed() * 2.f); }
        }
        // others; bigger
        << BonusType {
            .uid = 0, .duration = 300, .affects = BonusType::OTHERS, .icon = "Bi",
            .prologue = NULL, .update = NULL, .epilogue = NULL,
            .prologueTrace = [](PlayerTrace *trace) { trace->setWidth(trace->width() * 2.f); },
            .updateTrace = NULL,
            .epilogueTrace = [](PlayerTrace *trace) { trace->setWidth(trace->width() / 2.f); }
        }
        // self; square turns (snakey)
        << BonusType {
            .uid = 100, .duration = 600, .affects = BonusType::SELF, .icon = "Sq",
            .prologue = NULL, .update = NULL, .epilogue = NULL,
            .prologueTrace = [](PlayerTrace *trace) { trace->setSquareTurns(true); },
            .updateTrace = NULL,
            .epilogueTrace = [](PlayerTrace *trace) { trace->setSquareTurns(false); }
        }
        // others; square turns (snakey)
        << BonusType {
            .uid = 100, .duration = 600, .affects = BonusType::OTHERS, .icon = "Sq",
            .prologue = NULL, .update = NULL, .epilogue = NULL,
            .prologueTrace = [](PlayerTrace *trace) { trace->setSquareTurns(true); },
            .updateTrace = NULL,
            .epilogueTrace = [](PlayerTrace *trace) { trace->setSquareTurns(false); }
        }
        // self; clear tail
        << BonusType {
            .uid = 1000, .duration = 0, .affects = BonusType::SELF, .icon = "Cl",
            .prologue = NULL,
            .update = NULL,
            .epilogue = NULL,
            .prologueTrace = [](PlayerTrace* trace) { trace->clearTail(); },
            .updateTrace = NULL,
            .epilogueTrace = NULL,
        }
        // all; clear tails
        << BonusType {
            .uid = 1000, .duration = 0, .affects = BonusType::ALL, .icon = "Cl",
            .prologue = NULL,
            .update = NULL,
            .epilogue = NULL,
            .prologueTrace = [](PlayerTrace* trace) { trace->clearTail(); },
            .updateTrace = NULL,
            .epilogueTrace = NULL,
        }
        // all; global; reduce scene
        << BonusType {
            .uid = -10, .duration = 1000, .affects = BonusType::ALL, .icon = "Re",
            .prologue = NULL,
            .update = [] () {
                GameScene *scene = Engine::instance()->gameScene();
                scene->setSceneRect(scene->sceneRect().adjusted(0.1, 0.1, -0.1, -0.1));
            },
            .epilogue = NULL,
            .prologueTrace = NULL,
            .updateTrace = NULL,
            .epilogueTrace = NULL,
        }
;
