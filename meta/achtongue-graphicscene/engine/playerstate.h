#ifndef PLAYERSTATE_H
#define PLAYERSTATE_H

#include <QtCore>

struct PlayerState {
    qreal width, speed, direction;   
    QColor color;
};

#endif // PLAYERSTATE_H
