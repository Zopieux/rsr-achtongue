#ifndef PROTOCOL_H
#define PROTOCOL_H
#include <stdlib.h>
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QPoint>
#include <QDebug>
#include <QColor>

class protocol
{   

    static int messageLength;
public:
    protocol();
    static int getMessageLength();
    // message creation
    static QByteArray action(int MOVE,int xPos,int yPos,int playerID); //0
    static QByteArray bonus(int,int,int); //1
    static QByteArray helloServer(QString); //2
    static QByteArray gameParams(int, int, qreal startx, qreal starty, QColor color); //3
    static QByteArray joinRoom(int); //4
    static QByteArray gameStart(int xPos,int yPos, qreal direction); //5
    static QByteArray gameOver(); //6
    static QByteArray roomFull(); //7
    // message usage
    static QString getUsernameFromHelloServer(QByteArray);
    static int getIdFromParams(QByteArray);
    static int getSndPortFromParams(QByteArray);
    static int getIdFromJoinRoom(QByteArray);
    static qreal getDirectionFromStartGame(QByteArray);
    static QPoint getPositionFromStartGame(QByteArray);
    static QColor getColor(QByteArray);
    // message type
    static bool isAction(QByteArray);
    static bool isBonus(QByteArray);
    static bool isHelloServer(QByteArray);
    static bool isGameParams(QByteArray);
    static bool isJoinRoom(QByteArray);
    static bool isGameStart(QByteArray);
    static bool isGameOver(QByteArray);
    static bool isRoomFull(QByteArray);

private:    
    static bool check(QByteArray,int);


};

#endif // PROTOCOL_H
