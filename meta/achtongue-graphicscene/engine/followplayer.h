#ifndef FOLLOWPLAYER_H
#define FOLLOWPLAYER_H

#include <QObject>
#include <QTimer>
#include "engine/player.h"

class FollowPlayer : public QObject, public Player
{
    Q_OBJECT
public:
    explicit FollowPlayer(const QString &name, const PlayerColors &colors, Player *following);
    ~FollowPlayer();

signals:

private slots:
    void follow();

private:
    PlayerTrace *following;
    QTimer *actTimer;
    qreal random;
};

#endif // FOLLOWPLAYER_H
