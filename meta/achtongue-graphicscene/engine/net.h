#ifndef NET_H
#define NET_H

#include <QThread>
#include <QMutex>
#include <QByteArray>
#include <engine/protocol.h>
#include <QQueue>
#include <QHostAddress>
#include <QTcpSocket>
#include <QString>

class Net : public QObject
{
    Q_OBJECT
public:
    explicit Net(QObject *parent = 0, QString server="localhost", int port=12345);
    QByteArray fetchCommand();
    QByteArray data;
    void sendAction(int,int,int);
    bool newCommand();    
    int getRemoteID();
    void registerOnServer(QString name);
    void joinRoom(int id, int port);
    void doSetup(int,int);

signals:
    void readyToEnterTheRoom(int,int);
    void connectionTimeout();

public slots:
//    void run();
    void readData();
    void sendData(QByteArray);

private:
    QMutex mutex;
    QQueue<QByteArray> queue;
    int port;
    int roomPort;
    QString server;
    QTcpSocket* socket;
    int playerId;
    bool isReady = false;
};

#endif // NET_H
