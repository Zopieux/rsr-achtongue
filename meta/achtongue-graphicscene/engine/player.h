#ifndef PLAYER_H
#define PLAYER_H

#include <QList>
#include "const.h"
#include "graphics/playertrace.h"

class PlayerTrace;

class Player
{
public:
    Player(const QString &name, const PlayerColors &colors);
    virtual ~Player();

    inline QString name() const
    {
        return mName;
    }
    inline int traceCount() const
    {
        return mTraces.size();
    }
    inline PlayerTrace *traceAt(int i) const
    {
        return mTraces.at(i);
    }
    inline QList<PlayerTrace *> traces() const
    {
        return mTraces;
    }
    bool alive();

    void setName(const QString &name);
    void addTrace();
    void init();

    // shortcuts that applies on all traces
    void setTracing(bool tracing);
    void setPos(const QPointF &pos);
    void setDirection(qreal direction);
    void setWidth(qreal width);
    void setSpeed(qreal speed);
    void setTurnRadius(qreal turnRadius);
    void setColors(const PlayerColors &colors);
    void setMove(PlayerMove move);
    void setMoving(bool moving);
    void clearTail();
    void update(qint64 tick);

private:
    QString mName;
    PlayerColors mColors;
    QList<PlayerTrace *> mTraces;
    bool mAlive;
};

#endif // PLAYER_H
