#include "engine/bonus.h"
#include "engine/bonusmanager.h"
#include "engine/engine.h"
#include "utils.h"
#include <QDebug>

BonusManager::BonusManager(QObject *parent) :
    QObject(parent)
{
}

BonusManager::~BonusManager()
{
    while (!mBonusToPlayers.isEmpty()) {
        Bonus *bonus = mBonusToPlayers.lastKey();
        mBonusToPlayers.remove(bonus);
        delete bonus;
    }
    // FIXME: delete mGlobalBonus items
    mGlobalBonus.clear();
    mPlayerToBonus.clear();
}

QSet<Bonus *> BonusManager::bonusForPlayer(PlayerTrace *trace) const
{
    return mPlayerToBonus.value(trace);
}

void BonusManager::update(tick_t tick)
{
    foreach (Bonus *bonus, mGlobalBonus) {
        bonus->tickGlobal();
    }
    foreach (Bonus *bonus, mBonusToPlayers.keys()) {
        bonus->tick();
    }
    foreach (Player *player, Engine::instance()->players()) {
        updatePlayer(player, tick);
    }
    QMutableSetIterator<Bonus*> itGlobal(mGlobalBonus);
    while (itGlobal.hasNext()) {
        Bonus *bonus = itGlobal.next();
        if (bonus->done()) {
            qDebug() << "GLOBAL bonus done";
            itGlobal.remove();
            delete bonus;
        }
    }
    QMutableMapIterator<Bonus *, QSet<PlayerTrace *> > it(mBonusToPlayers);
    while (it.hasNext()) {
        it.next();
        Bonus *bonus = it.key();
        if (bonus->done()) {
            qDebug() << "bonus done";
            foreach (Player *player, Engine::instance()->players()) {
                // remove from set
                foreach(PlayerTrace *trace, player->traces())
                mPlayerToBonus[trace].remove(bonus);
            }
            // remove pair from map
            it.remove();
            delete bonus;
        }
    }
}

void BonusManager::updatePlayer(Player *player, tick_t tick)
{
    foreach (PlayerTrace *trace, player->traces()) {
        // Check collision with heads
        updatePlayerTrace(trace, tick);
        // Update head timers
        trace->updateBonus();
    }
}

void BonusManager::updatePlayerTrace(PlayerTrace *trace, tick_t tick)
{
    Player *player = trace->player();
    // Check for collisions
    QList<QGraphicsItem *> items = Engine::instance()->gameScene()->items(trace->rect());
    foreach (QGraphicsItem *gitem, items) {
        BonusItem *bonusItem = dynamic_cast<BonusItem *>(gitem);
        if (bonusItem == NULL)
            continue;
        // Compute precise intersection
        QPainterPath p, q;
        p.addEllipse(bonusItem->rect());
        q.addEllipse(trace->rect());
        if (!p.intersects(q))
            // No actual collision
            continue;
        playerBonusPick(trace, bonusItem);
    }
}

Bonus *BonusManager::playerHasEffect(PlayerTrace *trace, const BonusType &type) const
{
    foreach (Bonus *bonus, mPlayerToBonus[trace])
        if (bonus->typeUid() == type.uid)
            return bonus;
    return NULL;
}

Bonus *BonusManager::globalHasEffect(const BonusType &type) const
{
    foreach (Bonus *bonus, mGlobalBonus)
        if (bonus->typeUid() == type.uid)
            return bonus;
    return NULL;
}

void BonusManager::playerBonusPick(PlayerTrace *trace, BonusItem *bonusItem)
{
    Player *const player = trace->player();
    qDebug() << "collision of" << player->name() << "with" << bonusItem;
    // extract bonus from item
    const BonusType &bonusType = bonusItem->bonusType();
    // remove BonusItem from scene and free it
    Engine::instance()->gameScene()->removeItem(bonusItem);
    delete bonusItem;
    Bonus *already, *bonus;
    if (bonusType.uid < 0) {
        if (!bonusType.stackable() && NULL != (already = globalHasEffect(bonusType))) {
            qDebug() << "GLOBAL bonus RESET" << bonusType.icon << already;
            already->reset();
        } else {
            qDebug() << "new GLOBAL bonus" << bonusType.icon;
            mGlobalBonus.insert(new Bonus(bonusType));
        }
    }
    if (bonusType.affects == BonusType::SELF) {
        if (!bonusType.stackable() && NULL != (already = playerHasEffect(trace, bonusType))) {
            qDebug() << "bonus RESET for" << player->name() << bonusType.icon << already;
            already->reset();
        } else {
            qDebug() << "new bonus for" << player->name() << "type" << bonusType.icon;
            bonus = new Bonus(bonusType, trace);
            mPlayerToBonus[trace].insert(bonus);
            mBonusToPlayers[bonus].insert(trace);
        }
    } else {
        bool excludeSelf = bonusType.affects == BonusType::OTHERS;
        foreach (Player *playerit, Engine::instance()->players()) {
            if (excludeSelf && playerit == player)
                continue;
            qDebug() << "new bonus for" << playerit->name() << "type" << bonusType.icon;
            foreach(PlayerTrace *traceit, playerit->traces()) {
                if (!bonusType.stackable() && NULL != (already = playerHasEffect(traceit, bonusType))) {
                    qDebug() << "bonus RESET for" << playerit->name() << bonusType.icon << already;
                    already->reset();
                } else {
                    bonus = new Bonus(bonusType, traceit);
                    mPlayerToBonus[traceit].insert(bonus);
                    mBonusToPlayers[bonus].insert(traceit);
                }
            }
        }
    }
}

BonusItem *BonusManager::spawnRandomBonus()
{
    // TODO: receive from server
    Q_ASSERT(Bonus::BonusTypes.length() > 0);
    const QRectF &rect = Engine::instance()->gameScene()->sceneRect();
    const BonusType &type = Bonus::BonusTypes.at(qrand() % Bonus::BonusTypes.length());
//    DEBUG
//    const BonusType &type = BonusTypes.last();
    BonusItem *bonusItem = new BonusItem(type);
    bonusItem->setPos(Utils::qrandRange(BONUS_SIZE.width(), rect.width() - BONUS_SIZE.width()),
                      Utils::qrandRange(BONUS_SIZE.height(), rect.height() - BONUS_SIZE.height()));
    Engine::instance()->gameScene()->addItem(bonusItem);
    return bonusItem;
}
