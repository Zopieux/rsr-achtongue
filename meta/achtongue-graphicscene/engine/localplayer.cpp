#include "engine/localplayer.h"

LocalPlayer::LocalPlayer(const QString &name, const PlayerColors &colors,
                         Qt::Key leftKey, Qt::Key rightKey) :
    Player(name, colors), mLeftKey(leftKey), mRightKey(rightKey)
{
}

Qt::Key LocalPlayer::leftKey() const
{
    return mLeftKey;
}

void LocalPlayer::setLeftKey(const Qt::Key &value)
{
    mLeftKey = value;
}

Qt::Key LocalPlayer::rightKey() const
{
    return mRightKey;
}

void LocalPlayer::setRightKey(const Qt::Key &value)
{
    mRightKey = value;
}

bool LocalPlayer::handleKey(Qt::Key key, bool pressed)
{
    if (!(key == mLeftKey || key == mRightKey))
        return false;
    int keyIndex = mKeyStack.indexOf(key);
    int lastKey = mKeyStack.empty() ? 0 : mKeyStack.last();
    if (pressed && keyIndex == -1) {
        // key not yet pressed
        mKeyStack.append(key);
    } else if (!pressed && keyIndex != -1) {
        // remove it from stack
        mKeyStack.remove(keyIndex);
    }
    if (mKeyStack.isEmpty()) {
        // no more keys, go straight
        setMove(STRAIGHT);
        // send STRAIGHT to server
//        emit sendAction(STRAIGHT);
        return true;
    } else if ((key = mKeyStack.last()) != lastKey) {
        // effectively turn
        PlayerMove move = key == mLeftKey ? LEFT : RIGHT;
        setMove(move);
        // send move to server
//        emit sendAction(move);
        //QByteArray data = protocol.action(move,0,0,0);
        //emit newAction(data);
        return true;
    }
    return false;
}


