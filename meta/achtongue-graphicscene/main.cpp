#include "mainwindow.h"
#include <QApplication>
#include <QTime>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Seed rand generator
    qsrand(QTime::currentTime().msec());

    MainWindow w;
    w.show();

    return a.exec();
}
