#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainterPath>
#include <QPainter>

class GameScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GameScene(QObject *parent = 0);
    ~GameScene();
    inline QRectF sceneRect() const
    {
        return mSceneRect;
    }
    void setSceneRect(const QRectF &rect);
    void setSceneRect(qreal x, qreal y, qreal w, qreal h);

signals:

public slots:
    void init();
    void updateFrame();

private:
    class SceneFrame : public QGraphicsItem
    {
    public:
        explicit SceneFrame(const QColor &color);
        inline QRectF boundingRect() const;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
        void sceneChanged();
    private:
        QColor mColor;
    };
    SceneFrame *mFrame;
    QGraphicsPathItem *outerMask;
    QRectF mSceneRect;
};

#endif // GAMESCENE_H
