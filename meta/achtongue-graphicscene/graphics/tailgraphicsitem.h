#ifndef TAILGRAPHICSITEM_H
#define TAILGRAPHICSITEM_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QStack>
#include "graphics/playertrace.h"
#include "const.h"

class PlayerTrace;


class TailGraphicsItem : public QObject
{
    Q_OBJECT
public:
    explicit TailGraphicsItem(PlayerTrace *playerTrace, QObject *parent = 0);
    ~TailGraphicsItem();
    void playerChanged();
    inline bool active()
    {
        return mActive;
    }

public slots:
    void start(const QPointF &pos, bool squareTurn=false);
    void start(bool squareTurn=false);
    void stop();
    void clear();

private:
    static qint64 MAX_TICKS;

    class TailSegment
    {
    public:
        TailSegment(const PlayerTrace &playerTrace, const QPointF &pos, TailGraphicsItem *parent);
        virtual ~TailSegment();
        void incrementTick();
        inline bool empty()
        {
            return ticks == 0;
        }
        inline QGraphicsItem *item()
        {
            return mItem;
        }
        inline QPen pen() const
        {
            return QPen(color, width, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin);
        }
        void markDone();
        virtual void update() = 0;
        virtual void updatePen() = 0;
    protected:
        bool done;
        qint64 ticks;
        qreal width, speed, direction;
        QColor color;
        QPointF start;
        QGraphicsItem *mItem;
        TailGraphicsItem *parent;
    };

    class TailLine : public TailSegment
    {
    public:
        TailLine(const PlayerTrace &playerTrace, const QPointF &pos, TailGraphicsItem *parent);
        QGraphicsLineItem *item();
        void update();
        void updatePen();
    };
    class TailCurve : public TailSegment
    {
    public:
        TailCurve(const PlayerTrace &playerTrace, const QPointF &pos, TailGraphicsItem *parent);
        QGraphicsPathItem *item();
        void update();
        void updatePen();
    private:
        qreal turnRadius;
    };

    bool mActive;
    QRectF mBoundRect;
    PlayerTrace *mPlayerTrace;
    QStack<TailSegment *> mSegments;

};

#endif // TAILGRAPHICSITEM_H
