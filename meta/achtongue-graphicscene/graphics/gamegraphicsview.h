#ifndef GAMEGRAPHICSVIEW_H
#define GAMEGRAPHICSVIEW_H

#include <QGraphicsView>

class GameGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit GameGraphicsView(QWidget *parent = 0);
    bool event(QEvent *event);
    void setScene(QGraphicsScene *scene);

signals:
    void viewChanged();

public slots:
private:
};

#endif // GAMEGRAPHICSVIEW_H
