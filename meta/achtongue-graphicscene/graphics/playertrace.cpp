#include "graphics/playertrace.h"
#include "engine/engine.h"
#include <QDebug>
#include <QtMath>

PlayerTrace::PlayerTrace(const PlayerColors &colors, Player *player) :
    mPlayer(player),
    mPos(0, 0), mMoving(false), mTracing(false), mAlive(true), mSquareTurns(false),
    mColors(colors), mColorCycleIndex(0), mColorCycle(40), mColorCycleCounter(0),
    mMove(STRAIGHT), mState(
{
    .width = 8, .speed = 3, .direction = 0, .turnRadius = 40
}),
wasWrapping(false)
{
#ifdef DEBUG_TRACE
    debug = new QGraphicsPathItem;
    Engine::instance()->gameScene()->addItem(debug);
#endif
    mHead = new HeadGraphicsItem(this);
    mTail = new TailGraphicsItem(this);
    for (int i = 0; i < 3; i++) {
        HeadGraphicsItem *head = new HeadGraphicsItem(this);
        head->setVisible(false);
        Engine::instance()->gameScene()->addItem(head);
        mHeadWraps.append(head);
        TailGraphicsItem *tail = new TailGraphicsItem(this);
        mTailWraps.append(tail);
    }
    Engine::instance()->gameScene()->addItem(mHead);
}

PlayerTrace::PlayerTrace(PlayerTrace *trace) :
    PlayerTrace(trace->colors(), trace->player())
{
    setPos(trace->pos());
    mState = trace->state();
}

PlayerTrace::~PlayerTrace()
{
    while (!mTailWraps.isEmpty())
        delete mTailWraps.takeLast();
    while (!mHeadWraps.isEmpty()) {
        delete mHeadWraps.takeLast();
    }
    delete mHead;
    delete mTail;
}

void PlayerTrace::update(qint64 tick)
{
    if (!mAlive)
        return;

    bool stateChanged = !changes.isEmpty();
    while (!changes.isEmpty()) {
        Change *change = changes.dequeue();
        ChangeType t = change->type();
        switch (t) {
        case MOVING:
            mMoving = change->value().toBool();
            break;
        case TRACING:
            mTracing = change->value().toBool();
            break;
        case MOVE:
            mMove = change->value().value<PlayerMove>();
            break;
        case WIDTH:
            mState.width = change->value().toReal();
            break;
        case SPEED:
            mState.speed = change->value().toReal();
            break;
        case DIRECTION:
            mState.direction = change->value().toReal();
            break;
        case TURN_RADIUS:
            mState.turnRadius = change->value().toReal();
            break;
        case SQUARE_TURNS:
            mSquareTurns = change->value().toBool();
            break;
        case POSITION:
            mPos = change->value().value<QPointF>();
            break;        
        }
        delete change;
    }

    if (outside()) {
        setPos(oppositePoint(mPos));
        update(tick);
        return;
    }

    if (mColors.length() > 1) {
        mColorCycleCounter++;
        if (mColorCycleCounter >= mColorCycle) {
            mColorCycleCounter = 0;
            mColorCycleIndex++;
            mColorCycleIndex %= mColors.length();
            if (mTracing)
                stateChanged = true;
        }
    }

    if (mMoving) {
        if (stateChanged) {
            stopTrace();
            startTrace();
        }
        if (mMove != STRAIGHT) {
            if(mSquareTurns){
                stopTrace();
                mPos += mState.width / 2 * QPointF(qCos(mState.direction), qSin(mState.direction));
                mState.direction +=  (mMove == LEFT ? -M_PI_2 : M_PI_2);
                mPos += mState.width / 2 * QPointF(qCos(mState.direction), qSin(mState.direction));
                mMove = STRAIGHT;
                startTrace(true);
            }else{
                mState.direction += (mMove == LEFT ? 1 : - 1) * PlayerTrace::deltaForSpeedTurnRadius(mState);
            }
        }        
        mPos += mState.speed * QPointF(qCos(mState.direction), qSin(mState.direction));
        if (almostOutside() && mTracing) {
            wasWrapping = true;
            // get the 1 to 3 wrapped points
            // FIXME: 4: magic number
            const  QVector<QPointF> wrapped = wrappedPoints(mPos, 4 * mState.width);
            int len = wrapped.length();
            // hide useless ones
            for (int i = len; i < 3; i++) {
                mTailWraps.at(i)->stop();
                mHeadWraps.at(i)->setVisible(false);
            }
            // trace useful ones
            QPointF p;
            for (int i = 0; i < len; i++) {
                p = wrapped.at(i);
                mHeadWraps.at(i)->setVisible(true);
                mHeadWraps.at(i)->playerChanged(p);
                if (!mTailWraps.at(i)->active()) {
                    mTailWraps.at(i)->start(p);
                } else {
                    mTailWraps.at(i)->playerChanged();
                }
            }
        } else if (wasWrapping) {
            wasWrapping = false;
            foreach (HeadGraphicsItem *head, mHeadWraps) {
                head->setVisible(false);
            }
            foreach (TailGraphicsItem *tail, mTailWraps) {
//                tail->playerChanged();
                tail->stop();
            }
        }
        mTail->playerChanged();
        mHead->playerChanged();
    } else if (stateChanged) {
        // else not moving (but state changed)
        mHead->playerChanged();
    }
}

void PlayerTrace::updateBonus()
{
    mHead->playerChanged();
    foreach (HeadGraphicsItem *head, mHeadWraps) {
        head->playerChanged();
    }
}

void PlayerTrace::setColors(const QVector<QColor> &colors)
{
    mColorCycleIndex = 0;
    mColorCycleCounter = 0;
    mColors = colors;
}

void PlayerTrace::stopTrace()
{
    mTail->stop();
    foreach (TailGraphicsItem *tail, mTailWraps) {
        tail->stop();
    }
}

void PlayerTrace::startTrace(bool squareTurn)
{
    if (!mTracing)
        return;
    mTail->start(squareTurn);
    // FIXME: I'm not sure it's okay to do this here but if we don't there is missing paint. I guess.
    // FIXME: 4: magic number
    if (almostOutside()) {
        const  QVector<QPointF> wrapped = wrappedPoints(mPos, 4 * mState.width);
        for (int i = 0; i < wrapped.length(); i++) {
            mTailWraps.at(i)->start(wrapped.at(i));
        }
    }
}

QRectF PlayerTrace::rect() const
{
    QRectF r(QPointF(0, 0), QSizeF(mState.width, mState.width));
    r.moveCenter(mPos);
    return r;
}

bool PlayerTrace::almostOutside()
{
    // check if we're outside or if we'll be next frame (in each 3 directions)
    const QRectF &ref = rect();
    QRectF r1(ref), r2(ref), r3(ref);
    qreal delta = PlayerTrace::deltaForSpeedTurnRadius(mState);
    r1.moveCenter(ref.center() + mState.speed * QPointF(qCos(mState.direction - delta),
                  qSin(mState.direction - delta)));
    r2.moveCenter(ref.center() + mState.speed * QPointF(qCos(mState.direction),
                  qSin(mState.direction)));
    r3.moveCenter(ref.center() + mState.speed * QPointF(qCos(mState.direction + delta),
                  qSin(mState.direction + delta)));
    const QRectF &sr = Engine::instance()->gameScene()->sceneRect();
    bool outside = !sr.contains(ref) || !sr.contains(r1) || !sr.contains(r2) || !sr.contains(r3);
#ifdef DEBUG_TRACE
    QPainterPath p;
    p.moveTo(ref.center());
    p.lineTo(r1.center());
    p.moveTo(ref.center());
    p.lineTo(r2.center());
    p.moveTo(ref.center());
    p.lineTo(r3.center());
    debug->setPath(p);
    debug->setPen(QPen(outside ? Qt::blue : Qt::white, 0));
#endif
    return outside;
}

bool PlayerTrace::outside()
{
    return !Engine::instance()->gameScene()->sceneRect().intersects(rect());
}

QVector<QPointF> PlayerTrace::wrappedPoints(const QPointF &pos, qreal offset) const
{
    QVector<QPointF> points;
    const QRectF &sceneRect = Engine::instance()->gameScene()->sceneRect();
    bool    left = pos.x() - offset < sceneRect.left(),
            top = pos.y() - offset < sceneRect.top(),
            right = !left && pos.x() + offset > sceneRect.right(),
            bottom = !top && pos.y() + offset > sceneRect.bottom();
    if (left) {
        QPointF p(pos);
        p.setX(sceneRect.right() + pos.x() - sceneRect.left());
        points.append(p);
    }
    if (top) {
        QPointF p(pos);
        p.setY(sceneRect.bottom() + pos.y() - sceneRect.top());
        points.append(p);
    }
    if (right) {
        QPointF p(pos);
        p.setX(pos.x() - sceneRect.right() + sceneRect.left());
        points.append(p);
    }
    if (bottom) {
        QPointF p(pos);
        p.setY(pos.y() - sceneRect.bottom() + sceneRect.top());
        points.append(p);
    }
    if (left && top) {
        points.append(sceneRect.bottomRight() + pos);
    } else if (left && bottom) {
        points.append(QPointF(sceneRect.right() + pos.x(), pos.y() - sceneRect.bottom()));
    } else if (right && top) {
        points.append(QPointF(pos.x() - sceneRect.right(), sceneRect.bottom() + pos.y()));
    } else if (right && bottom) {
        points.append(pos - sceneRect.bottomRight());
    }
    Q_ASSERT(points.length() <= 3);
    return points;
}

QPointF PlayerTrace::oppositePoint(const QPointF &pos) const
{
    const QRectF &sceneRect = Engine::instance()->gameScene()->sceneRect();
    bool    left = pos.x() < sceneRect.left(),
            top = pos.y() < sceneRect.top(),
            right = !left && pos.x() > sceneRect.right(),
            bottom = !top && pos.y() > sceneRect.bottom();
    QPointF p(pos);
    if (left)
        p.setX(sceneRect.right() + pos.x() - sceneRect.left());
    if (top)
        p.setY(sceneRect.bottom() + pos.y() - sceneRect.top());
    if (right)
        p.setX(sceneRect.left() + pos.x() - sceneRect.right());
    if (bottom)
        p.setY(sceneRect.top() + pos.y() - sceneRect.bottom());
    return p;
}

void PlayerTrace::clearTail()
{
    mTail->clear();
    foreach (TailGraphicsItem *tail, mTailWraps)
        tail->clear();
}

void PlayerTrace::setPos(const QPointF &pos)
{
    changes.enqueue(new Change(POSITION, pos));
}

void PlayerTrace::setMoving(bool moving)
{
    if (mMoving == moving)
        return;
    changes.enqueue(new Change(MOVING, moving));
}

void PlayerTrace::setTracing(bool tracing)
{
    if (tracing == mTracing)
        return;
    changes.enqueue(new Change(TRACING, tracing));
}

void PlayerTrace::setWidth(qreal width)
{
    changes.enqueue(new Change(WIDTH, width));
}

void PlayerTrace::setSpeed(qreal speed)
{
    changes.enqueue(new Change(SPEED, speed));
}

void PlayerTrace::setDirection(qreal direction)
{
    if (mState.direction == direction)
        return;
    changes.enqueue(new Change(DIRECTION, direction));
}

void PlayerTrace::offsetDirection(qreal offset)
{
    if (!offset)
        return;
    setDirection(direction() + offset);
}

void PlayerTrace::setSquareTurns(bool squareTurns){
    if (mSquareTurns == squareTurns)
        return;
    changes.enqueue(new Change(SQUARE_TURNS, squareTurns));
}

void PlayerTrace::setTurnRadius(qreal turnRadius)
{
    // no need to stop trace if going straight, because will apply on next *turn* only!
    if (turnRadius == mState.turnRadius)
        return;
    if (mMove == STRAIGHT)
        mState.turnRadius = turnRadius;
    else
        changes.enqueue(new Change(TURN_RADIUS, turnRadius));
}

void PlayerTrace::setMove(PlayerMove move)
{
    if (move == mMove)
        return;
    changes.enqueue(new Change(MOVE, QVariant::fromValue<PlayerMove>(move)));
}
