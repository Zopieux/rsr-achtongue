#include "gamegraphicsview.h"
#include "const.h"
#include <QDebug>
#include <QEvent>
#include <QPaintEvent>

GameGraphicsView::GameGraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
    setBackgroundBrush(QBrush(BACKGROUND_COLOR));
    setFocusPolicy(Qt::NoFocus);
}

bool GameGraphicsView::event(QEvent *event)
{
    QEvent::Type type = event->type();
    if (type == QEvent::GraphicsSceneResize || type == QEvent::GraphicsSceneMove
            || type == QEvent::Resize) {
        emit viewChanged();
    }
    return QGraphicsView::event(event);
}

void GameGraphicsView::setScene(QGraphicsScene *scene)
{
    QGraphicsView::setScene(scene);
    setSceneRect(scene->sceneRect().adjusted(-BORDER_WIDTH, -BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH));
}
