#include "graphics/headgraphicsitem.h"
#include "engine/bonus.h"
#include "engine/engine.h"
#include <QtMath>
#include <QDebug>

qreal HeadGraphicsItem::penWidth = 5;
qreal HeadGraphicsItem::padding = 4;

HeadGraphicsItem::HeadGraphicsItem(PlayerTrace *player, QGraphicsItem *parent) :
    QGraphicsItem(parent), mPlayerTrace(player)
{
    setZValue(1);
}

qreal HeadGraphicsItem::arcSize(int len) const
{
    return mPlayerTrace->width() + 2 + len * (padding * 2 + penWidth);
}

QRectF HeadGraphicsItem::boundingRect() const
{
    const QSet<Bonus *> &bonus = Engine::instance()->bonusForPlayer(mPlayerTrace);
    qreal w = arcSize(bonus.size() + 1);
    QRectF r(0, 0, w, w);
    r.moveCenter(mPlayerTrace->pos());
    return r;
}

void HeadGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->save();
    // Draw head
    painter->setPen(Qt::NoPen);
    painter->setBrush(mPlayerTrace->color());
    if (mPlayerTrace->tracing())
        painter->drawChord(mBoundRect,
                           -qRadiansToDegrees(mPlayerTrace->direction() - M_PI_2) * 16,
                           -180 * 16);
    else
        painter->drawEllipse(mBoundRect);

    // Draw bonus timers
    const QSet<Bonus *> &bonusList = Engine::instance()->bonusForPlayer(mPlayerTrace);
    if (bonusList.isEmpty()) {
        painter->restore();
        return;
    }
    qreal w;
    int sweep;
    int deg = 16 * -qRadiansToDegrees(mPlayerTrace->direction());
    int i = 1;
    painter->setBrush(Qt::NoBrush);
    painter->setPen(QPen(QColor(250, 250, 250, 140), penWidth, Qt::SolidLine, Qt::FlatCap));
    QPointF center = mPlayerTrace->pos();
    foreach (Bonus *bonus, bonusList) {
        if (bonus->duration() == 0)
            continue;
        w = arcSize(i++);
        QRectF r(0, 0, w, w);
        r.moveCenter(center);
        sweep = qIntCast(16 * 360 * bonus->completion());
        painter->drawArc(r, deg, sweep);
    }
    painter->restore();
}

void HeadGraphicsItem::playerChanged()
{
    playerChanged(mPlayerTrace->pos());
}

void HeadGraphicsItem::playerChanged(const QPointF &pos)
{
    if (!isVisible())
        return;
    prepareGeometryChange();
    mBoundRect = QRectF(QPointF(0, 0), QSizeF(mPlayerTrace->width(), mPlayerTrace->width()));
    mBoundRect.moveCenter(pos);
}
