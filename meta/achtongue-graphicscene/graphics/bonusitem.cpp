#include "bonusitem.h"
#include <QDebug>

BonusItem::BonusItem(const BonusType &type, QGraphicsItem *parent) :
    QGraphicsItem(parent), mBonusType(type)
{
    setZValue(-20);
}

QRectF BonusItem::boundingRect() const
{
    return QRect(QPoint(-BONUS_SIZE.width() / 2, -BONUS_SIZE.height() / 2), BONUS_SIZE);
}

QRectF BonusItem::rect() const
{
    return boundingRect().translated(pos());
}

void BonusItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QFont f(painter->font());
    f.setPointSizeF(boundingRect().height() / 3);
    painter->save();
    switch (mBonusType.affects) {
    case BonusType::SELF:
        // green QColor(98, 167, 15)
        painter->setBrush(QColor(98, 167, 15));
        break;
    case BonusType::OTHERS:
        // red QColor(209, 28, 28)
        painter->setBrush(QColor(209, 28, 28));
        break;
    case BonusType::ALL:
        // blue QColor(27, 112, 199)
        painter->setBrush(QColor(27, 112, 199));
        break;
    }
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(boundingRect());
    painter->setPen(Qt::white);
    painter->setBrush(Qt::NoBrush);
    painter->setFont(f);
    // TODO symbol according to bonus->type()
    painter->drawText(boundingRect(), Qt::AlignCenter, mBonusType.icon);
    painter->restore();
}
