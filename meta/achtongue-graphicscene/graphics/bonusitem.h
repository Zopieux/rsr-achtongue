#ifndef BONUSITEM_H
#define BONUSITEM_H

#include <QGraphicsItem>
#include <QPainter>
#include "const.h"
#include "engine/bonus.h"

class BonusItem : public QGraphicsItem
{
public:
    BonusItem(const BonusType &type, QGraphicsItem *parent = 0);

    // QGraphicsItem interface
public:
    QRectF boundingRect() const;
    QRectF rect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    inline BonusType bonusType() const
    {
        return mBonusType;
    }

private:
    const BonusType &mBonusType;
};

#endif // BONUSITEM_H
