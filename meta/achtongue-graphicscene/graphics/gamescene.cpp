#include "graphics/gamescene.h"
#include "engine/engine.h"
#include "const.h"
#include <QGraphicsView>
#include <QDebug>

GameScene::GameScene(QObject *parent) :
    QGraphicsScene(parent)
{
    setSceneRect(QRectF(QPointF(0, 0), SCENE_SIZE));
    mFrame = new SceneFrame(Qt::white);
    outerMask = new QGraphicsPathItem;
    outerMask->setZValue(99);
    outerMask->setPen(Qt::NoPen);
    outerMask->setBrush(QBrush(BACKGROUND_COLOR));
}

GameScene::~GameScene()
{
    delete mFrame;
    delete outerMask;
}

void GameScene::setSceneRect(const QRectF &rect)
{
    mSceneRect = rect;
    QGraphicsScene::setSceneRect(rect);
}

void GameScene::setSceneRect(qreal x, qreal y, qreal w, qreal h)
{
    setSceneRect(QRectF(x, y, w, h));
}

void GameScene::init()
{
    addItem(mFrame);
    addItem(outerMask);
    connect(this, SIGNAL(sceneRectChanged(QRectF)), this, SLOT(updateFrame()));
    updateFrame();
}

void GameScene::updateFrame()
{
    const QRect &sr = sceneRect().toRect();
    QRect vr = views().first()->rect();
    vr.moveCenter(sr.center());
    QPainterPath p;
    p.addRect(vr);
    p.addRect(sr);
    outerMask->setPath(p);
    mFrame->sceneChanged();
}

GameScene::SceneFrame::SceneFrame(const QColor &color) :
    QGraphicsItem(), mColor(color)
{
    setZValue(100);
}

QRectF GameScene::SceneFrame::boundingRect() const
{
    return Engine::instance()->gameScene()->sceneRect();
}

void GameScene::SceneFrame::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                                  QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    const QRect &inner = boundingRect().toRect();
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, false);
    painter->setBrush(Qt::NoBrush);
    painter->setPen(QPen(mColor, BORDER_WIDTH, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    painter->drawRect(inner.adjusted(-BORDER_WIDTH / 2, -BORDER_WIDTH / 2, BORDER_WIDTH / 2,
                                     BORDER_WIDTH / 2));
    painter->restore();
}

void GameScene::SceneFrame::sceneChanged()
{
    prepareGeometryChange();
}
