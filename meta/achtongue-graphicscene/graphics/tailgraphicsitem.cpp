#include "graphics/tailgraphicsitem.h"
#include "engine/engine.h"
#include <QtMath>
#include <QPainterPath>
#include <QGraphicsEffect>
#include <QDebug>

qint64 TailGraphicsItem::MAX_TICKS = 400;

TailGraphicsItem::TailGraphicsItem(PlayerTrace *playerTrace, QObject *parent) :
    QObject(parent), mActive(false), mPlayerTrace(playerTrace)
{
}

TailGraphicsItem::~TailGraphicsItem()
{
    stop();
    clear();
}

void TailGraphicsItem::playerChanged()
{
    if (!mActive || mSegments.isEmpty())
        return;
    mSegments.top()->incrementTick();
}

void TailGraphicsItem::start(bool squareTurn)
{
    start(mPlayerTrace->pos(), squareTurn);
}

void TailGraphicsItem::start(const QPointF &pos, bool squareTurn)
{
    TailSegment *seg;
    if (mPlayerTrace->move() == STRAIGHT) {
        seg = new TailLine(*mPlayerTrace, pos, this);
    } else {
        if (squareTurn) {
            // add a damn quarter pie
        }
        seg = new TailCurve(*mPlayerTrace, pos, this);
    }
    mSegments.push(seg);
    Engine::instance()->gameScene()->addItem(seg->item());
    mActive = true;
}

void TailGraphicsItem::stop()
{
    if (!mSegments.empty()) {
        // maybe the finished segment is outside the screen!
        // if so, delete it
        TailSegment *segment = mSegments.top();
        const QRectF &rect = segment->item()->boundingRect();
        if (segment->empty() || (!Engine::instance()->gameScene()->sceneRect().contains(rect)
                                 && !Engine::instance()->gameScene()->sceneRect().intersects(rect))) {
            mSegments.pop();
            delete segment;
            goto endStop;
        }
        mSegments.top()->markDone();
    }
endStop:
    mActive = false;
}

void TailGraphicsItem::clear()
{
    bool wasActive = mActive;
    stop();
    while (!mSegments.isEmpty()) {
        delete mSegments.pop();
    }
    if (wasActive)
        start();
}

TailGraphicsItem::TailSegment::TailSegment(const PlayerTrace &playerTrace, const QPointF &pos,
        TailGraphicsItem *parent) :
    done(false), ticks(0), width(playerTrace.state().width), speed(playerTrace.state().speed),
    direction(playerTrace.state().direction), color(playerTrace.color()),
    start(pos), parent(parent)
{
}

TailGraphicsItem::TailSegment::~TailSegment()
{
    Engine::instance()->gameScene()->removeItem(mItem);
    delete mItem;
}

void TailGraphicsItem::TailSegment::incrementTick()
{
    ticks++;
    bool wasActive = parent->mActive;
    if (ticks > MAX_TICKS) {
        qDebug() << "force new segment cause too many ticks";
        this->parent->stop();
        if (wasActive)
            this->parent->start();
    }
    update();
}

void TailGraphicsItem::TailSegment::markDone()
{
    if (done)
        return;
    done = true;
    updatePen();
    update();
}

TailGraphicsItem::TailLine::TailLine(const PlayerTrace &playerTrace, const QPointF &pos,
                                     TailGraphicsItem *parent) :
    TailSegment(playerTrace, pos, parent)
{
    mItem = new QGraphicsLineItem(QLineF(start, start));
    mItem->setZValue(-1);
    ((QGraphicsLineItem *) mItem)->setPen(pen());
    update();
}

void TailGraphicsItem::TailLine::updatePen()
{
#ifdef DEBUG_TRACE
    QPen pen2(pen());
    pen2.setColor(pen2.color().light(60));
    ((QGraphicsLineItem *) mItem)->setPen(pen2);
//    mItem->setGraphicsEffect(new QGraphicsBlurEffect);
#endif
}

void TailGraphicsItem::TailLine::update()
{
    QPointF stop = start + ticks * speed * QPointF(qCos(direction), qSin(direction));
    ((QGraphicsLineItem *) mItem)->setLine(QLineF(start, stop));
}

TailGraphicsItem::TailCurve::TailCurve(const PlayerTrace &player, const QPointF &pos,
                                       TailGraphicsItem *parent) :
    TailSegment(player, pos, parent),
    turnRadius((player.move() == LEFT ? 1 : -1) * player.state().turnRadius)
{
    QPainterPath path;
    path.moveTo(start);
    mItem = new QGraphicsPathItem(path);
    mItem->setZValue(-1);
    ((QGraphicsPathItem *) mItem)->setPen(pen());
    update();
}

void TailGraphicsItem::TailCurve::updatePen()
{
#ifdef DEBUG_TRACE
    QPen pen2(pen());
    pen2.setColor(pen2.color().light(60));
    ((QGraphicsPathItem *) mItem)->setPen(pen2);
//    mItem->setGraphicsEffect(new QGraphicsBlurEffect);
#endif
}

void TailGraphicsItem::TailCurve::update()
{
    QPainterPath path;
    qint64 ticker;
    QPointF step = start;
    qreal dir = direction;
    qreal dirAdd = PlayerTrace::deltaForSpeedTurnRadius(speed, turnRadius);
    // We move back a bit to trace the first line
    // TODO We may be able to do better
    path.moveTo(start + 1 * QPointF(qCos(dir + M_PI), qSin(dir + M_PI)));
    path.lineTo(start);
    // TODO Use constant delta
    for (ticker = 0; ticker < ticks; ++ticker) {
        dir += dirAdd;
        step += speed * QPointF(qCos(dir), qSin(dir));
        path.lineTo(step);
    }
    // We move further a bit to trace the last line
    // TODO We may be able to do better
    path.lineTo(step + 1 * QPointF(qCos(dir), qSin(dir)));
    ((QGraphicsPathItem *) mItem)->setPath(path);
}
