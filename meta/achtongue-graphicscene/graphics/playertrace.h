
#ifndef PLAYERTRACE_H
#define PLAYERTRACE_H

#include <QGraphicsScene>
#include <QQueue>
#include <QtMath>
#include "const.h"
#include "graphics/headgraphicsitem.h"
#include "graphics/tailgraphicsitem.h"

class Player;
class HeadGraphicsItem;
class TailGraphicsItem;
class HeadBonusTimers;
class Bonus;


class PlayerTrace
{
public:
    struct State {
        qreal width, speed, direction, turnRadius;
    };

    explicit PlayerTrace(const PlayerColors &colors, Player *parent);
    PlayerTrace(PlayerTrace *trace);
    ~PlayerTrace();

    inline Player *player()
    {
        return mPlayer;
    }
    inline State state() const
    {
        return mState;
    }
    inline QColor color() const
    {
        return mColors.at(mColorCycleIndex);
    }
    inline QPointF pos() const
    {
        return mPos;
    }
    inline QVector<QColor> colors() const
    {
        return mColors;
    }
    inline PlayerMove move() const
    {
        return mMove;
    }
    inline bool alive() const
    {
        return mAlive;
    }
    inline bool tracing() const
    {
        return mTracing;
    }
    inline qreal direction() const
    {
        return mState.direction;
    }
    inline qreal width() const
    {
        return mState.width;
    }
    inline qreal speed() const
    {
        return mState.speed;
    }
    inline qreal turnRadius() const
    {
        return mState.turnRadius;
    }
    QRectF rect() const;

    bool isSquareMode();
    void update(qint64 tick);
    void updateBonus();
    void clearTail();    
    void setTracing(bool tracing);
    void setPos(const QPointF &pos);
    void setWidth(qreal width);
    void setSpeed(qreal speed);
    void setTurnRadius(qreal turnRadius);
    void setColors(const PlayerColors &colors);
    void setMove(PlayerMove move);
    void setMoving(bool moving);
    void setDirection(qreal direction);
    void setSquareTurns(bool squareMode);
    void offsetDirection(qreal offset);

    // TODO: this could be stored in a hashtable (but not really useful seriously)
    static inline qreal deltaForSpeedTurnRadius(qreal speed, qreal turnRadius)
    {
        return (turnRadius > 0 ? -1 : 1) * 2 * qAsin(speed / (2 * qAbs(turnRadius)));
    }
    static inline qreal deltaForSpeedTurnRadius(const State &state)
    {
        return deltaForSpeedTurnRadius(state.speed, state.turnRadius);
    }

protected:
    void stopTrace();
    void startTrace(bool squareTurn=false);

private:
    enum ChangeType {
        MOVING, TRACING, MOVE, WIDTH, SPEED, DIRECTION, TURN_RADIUS, POSITION, SQUARE_TURNS
    };

    class Change
    {
    public:
        Change(ChangeType type, const QVariant &value = QVariant()) :
            mType(type), mValue(value) {}
        ChangeType type() const
        {
            return mType;
        }
        QVariant value() const
        {
            return mValue;
        }
    private:
        ChangeType mType;
        QVariant mValue;
    };

    Player *mPlayer;
    QPointF mPos;
    bool mMoving, mTracing;
    bool mAlive;
    bool mSquareTurns;
    PlayerColors mColors;
    int mColorCycleIndex;
    qint64 mColorCycle, mColorCycleCounter;
    PlayerMove mMove;
    State mState;
    HeadGraphicsItem *mHead;
    QVector<HeadGraphicsItem *> mHeadWraps;
    TailGraphicsItem *mTail;
    QVector<TailGraphicsItem *> mTailWraps;
#ifdef DEBUG_TRACE
    QGraphicsPathItem *debug;
#endif
    bool wasWrapping;
    QQueue<Change *> changes;

    inline bool almostOutside();
    inline bool outside();
    QPointF oppositePoint(const QPointF &pos) const;
    QVector<QPointF> wrappedPoints(const QPointF &pos, qreal offset = 0) const;
};

#endif // PLAYERTRACE_H
