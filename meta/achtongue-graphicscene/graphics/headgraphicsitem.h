#ifndef HEADGRAPHICSITEM_H
#define HEADGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QPainter>
#include "engine/player.h"
#include "const.h"

class PlayerTrace;
class Bonus;

class HeadGraphicsItem : public QGraphicsItem
{
public:
    explicit HeadGraphicsItem(PlayerTrace *mPlayerTrace, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void playerChanged();
    void playerChanged(const QPointF &pos);

private:
    inline qreal arcSize(int len) const;
    static qreal penWidth, padding;
    QRectF mBoundRect;
    PlayerTrace *mPlayerTrace;
};

#endif // HEADGRAPHICSITEM_H
