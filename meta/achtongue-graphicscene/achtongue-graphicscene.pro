#-------------------------------------------------
#
# Project created by QtCreator 2014-11-23T15:33:54
#
#-------------------------------------------------

QT       += core gui multimedia

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread
TARGET = achtongue-graphicscene
TEMPLATE = app
#DEFINES += DEBUG_TRACE

SOURCES += main.cpp\
        mainwindow.cpp \
    graphics/headgraphicsitem.cpp \
    graphics/tailgraphicsitem.cpp \
    graphics/playertrace.cpp \
    graphics/gamescene.cpp \
    engine/followplayer.cpp \
    engine/localplayer.cpp \
    engine/player.cpp \
    graphics/bonusitem.cpp \
    engine/bonus.cpp \
    engine/bonusmanager.cpp \
    engine/engine.cpp \
    graphics/gamegraphicsview.cpp \
    engine/protocol.cpp \
    engine/net.cpp

HEADERS  += mainwindow.h \
    const.h \
    graphics/headgraphicsitem.h \
    graphics/tailgraphicsitem.h \
    graphics/playertrace.h \
    graphics/gamescene.h \
    engine/playerstate.h \
    engine/followplayer.h \
    engine/localplayer.h \
    engine/player.h \
    graphics/bonusitem.h \
    engine/bonus.h \
    engine/bonusmanager.h \
    engine/engine.h \
    utils.h \
    graphics/gamegraphicsview.h \
    engine/protocol.h \
    engine/net.h

FORMS    += mainwindow.ui

#RESOURCES += \
#    res.qrc
