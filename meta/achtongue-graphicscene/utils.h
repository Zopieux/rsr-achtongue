#ifndef UTILS_H
#define UTILS_H

#include <QtGlobal>

namespace Utils
{

static int qrandRange(int low, int high)
{
    return qrand() % ((high + 1) - low) + low;
}

}

#endif // UTILS_H
