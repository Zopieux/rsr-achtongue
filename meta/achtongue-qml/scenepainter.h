#ifndef SCENEPAINTER_H
#define SCENEPAINTER_H

#include <QObject>
#include <QtQuick/QQuickPaintedItem>

class ScenePainter : public QQuickPaintedItem
{
    Q_OBJECT

public:
    explicit ScenePainter(QQuickItem *parent = 0);
    QString name() const;

    void paint(QPainter *painter);

signals:

public slots:

};

#endif // SCENEPAINTER_H
