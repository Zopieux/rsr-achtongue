import QtQuick 2.0

Item {
    property bool tracing: true
    property bool activeTrace: false
    property string move: "S"
    property real twidth: 8
    property real speed: 1 / 10
    property real dir: 0
    property real turnRadius: 1
    property color color

    property int keyLeft
    property int keyRight
    property var keyStack: []

    property point lastStart
    property real lastDir: 0
    property int noTraceCount: 0

    function init(x_, y_, dir_, fpratio) {
        x = x_;
        y = y_;
        dir = dir_;
        speed *= fpratio;
        turnRadius *= fpratio;
        startTrace();
        drawFg();
    }

    function showPos(x, y) {
        var cc = canvasDebug.ctx();
        cc.save();
        cc.fillStyle = 'white';
        cc.beginPath();
        cc.moveTo(x, y);
        cc.arc(x, y, 1, 0, 360);
        cc.fill();
        cc.restore();
    }

    function showDir(x, y, l, dir) {
        var cc = canvasDebug.ctx(), x1, y1, dd;
        cc.save();
        cc.strokeStyle = 'white';
        cc.lineWidth = 1;
        cc.beginPath();
        cc.moveTo(x, y);
        x += l * Math.cos(dir);
        y += l * Math.sin(dir);
        cc.lineTo(x, y);
        dd = Math.PI + dir + Math.PI / 4;
        x1 = x + 10 * Math.cos(dd);
        y1 = y + 10 * Math.sin(dd);
        cc.lineTo(x1, y1);
        dd = Math.PI + dir - Math.PI / 4;
        x1 = x + 10 * Math.cos(dd);
        y1 = y + 10 * Math.sin(dd);
        cc.moveTo(x, y);
        cc.lineTo(x1, y1);
        cc.stroke();
        cc.restore();
    }

    function drawArc(ctx, startx, starty, startdir, enddir, _speed, _radius) {
        var sign = (startdir > enddir) ? -1 : 1;
        var e = sign * 2 * Math.asin(_speed / (2 * _radius));
        // fix first segment
        var xx, yy;
        // go back a bit
        xx = startx + 0.5 * Math.cos(startdir+Math.PI);
        yy = starty + 0.5 * Math.sin(startdir+Math.PI);
        ctx.moveTo(xx, yy);
        // go forward a bit
        xx = startx + 0.5 * Math.cos(startdir);
        yy = starty + 0.5 * Math.sin(startdir);
        ctx.lineTo(xx, yy);
        while (Math.abs(startdir - enddir) > 0.001) {
            startdir += e;
            startx += _speed * Math.cos(startdir);
            starty += _speed * Math.sin(startdir);
            ctx.lineTo(startx, starty);
        }
    }

    function stopTrace() {
        if(tracing && activeTrace) {
            // flush current to persistent
            var ctx = canvasBg.ctx();
            ctx.save();
            ctx.strokeStyle = color;
            ctx.lineWidth = twidth;
            ctx.beginPath();
            // go back a bit
            var xx = lastStart.x + 0.5 * Math.cos(dir+Math.PI);
            var yy = lastStart.y + 0.5 * Math.sin(dir+Math.PI);
            ctx.moveTo(xx, yy);
            if(move == "S") {
                ctx.lineTo(x, y);
            } else {
                drawArc(ctx, lastStart.x, lastStart.y, lastDir, dir, speed, turnRadius);
            }
            ctx.stroke();
            ctx.restore();
        }
    }

    function startTrace() {
        lastStart = Qt.point(x, y);
        lastDir = dir;
    }

    function handleKey(key, pressed) {
        if (key !== keyLeft && key !== keyRight)
            // not our business
            return;
        var lastKey = keyStack[keyStack.length - 1];
        var keyFound = keyStack.indexOf(key) > -1;
        if (pressed && !keyFound) {
            // key not yet pressed
            keyStack.push(key);
        } else if (!pressed && keyFound) {
            // remove it from stack
            keyStack.splice(keyStack.indexOf(key), 1);
        }
        key = keyStack[keyStack.length - 1];
        if (keyStack.length === 0) {
            // no more keys, go straight
            stopTrace();
            move = "S";
            startTrace();
        } else if (lastKey !== key) {
            // effectively turn
            stopTrace();
            move = key === keyLeft ? "L" : "R";
            startTrace();
        }
    }

    function drawFg() {
        // update fg surface
        var ctx = canvasFg.getContext("2d");
        ctx.save();
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.arc(x, y, twidth / 2, 0, 360);
        ctx.fill();
        ctx.restore();
        if (tracing && activeTrace) {
            ctx.save();
            ctx.strokeStyle = color;
            ctx.lineWidth = twidth;
            ctx.beginPath();
            // go back a bit
            var xx = lastStart.x + 0.5 * Math.cos(dir+Math.PI);
            var yy = lastStart.y + 0.5 * Math.sin(dir+Math.PI);
            ctx.moveTo(xx, yy);
            if(move === "S") {
                ctx.lineTo(x, y);
            } else if(move === "L" || move === "R") {
                drawArc(ctx, lastStart.x, lastStart.y, lastDir, dir, speed, turnRadius);
            }
            ctx.stroke();
            ctx.restore();
        }
    }

    function update() {
        if(move === "R") {
            dir += 2 * Math.asin(speed / (2 * turnRadius));
        } else if(move === "L") {
            dir -= 2 * Math.asin(speed / (2 * turnRadius));
        }
        x += speed * Math.cos(dir);
        y += speed * Math.sin(dir);

        drawFg();

        // hole?
        if (tracing) {
            if (Math.random() < 1. / 100 * (8 / twidth)) {
                stopTrace();
                tracing = false;
                noTraceCount = 2 * twidth / speed;
            }
        } else {
            if (noTraceCount-- <= 0) {
                tracing = true;
                startTrace();
            }
        }
    }
}
