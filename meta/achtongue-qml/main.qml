import QtQuick 2.3
import QtQuick.Window 2.2

Window {
    id: main
    visible: true
    width: 700
    height: 700

    property int framerate: 30
    property real fpratio: 1000. / framerate

    Item {
        objectName: "control"
        id: control
        property bool inited: false
        signal readyToInit()
        function init() {
            if (inited)
                return;
            if (!(canvasFg.available && canvasBg.available && canvasDebug.available))
                return;
            inited = true;
            readyToInit();
            return;
            var m = 20;
            player1.init(
                        m + Math.random() * (sceneRect.width - 2 * m),
                        m + Math.random() * (sceneRect.width - 2 * m),
                        Math.random() * Math.PI * 2);
            player2.init(
                        m + Math.random() * (sceneRect.height - 2 * m),
                        m + Math.random() * (sceneRect.height - 2 * m),
                        Math.random() *  Math.PI * 2);
            canvasFg.requestPaint();
            initTimer.start();
            startTraceTimer.start();
        }
    }

    Timer {
        id: initTimer
        interval: 1000
        repeat: false
        running: false
        onTriggered: {
            timerUpdate.start();
        }
    }

    Timer {
        id: startTraceTimer
        interval: 3000
        repeat: false
        running: false
        onTriggered: {
            // TODO: loop
//            player1.stopTrace();
//            player2.stopTrace();
//            player1.activeTrace = true;
//            player2.activeTrace = true;
//            player1.startTrace();
//            player2.startTrace();
        }
    }

    Timer {
        id: timerColorChange
        property int index: 0

        interval: 50 * fpratio
        repeat: true
        running: false
        onTriggered: {
            var colors = ["#697614", "#FDD041", "#C83231"];
            index++;
            index %= 3;
            player1.stopTrace();
            player1.color = colors[index];
            player1.startTrace();
        }
    }

    Timer {
        id: timerUpdate
        interval: fpratio
        running: false
        repeat: true
        onTriggered: {
            if(!(canvasBg.available && canvasFg.available))
                return
            var ctx = canvasFg.getContext("2d");
            ctx.clearRect(0, 0, sceneRect.width, sceneRect.height);
            player1.update();
            player2.update();
            // update canvas
            canvasFg.requestPaint();
            canvasBg.requestPaint();
        }
    }

    Item {
        id: inputProcessor
        focus: true
        Keys.onPressed: {
            if(event.isAutoRepeat) return;
            // TODO: loop
            player1.handleKey(event.key, true);
            player2.handleKey(event.key, true);
            if(event.key === Qt.Key_C) {
                canvasBg.clear();
                event.accepted = true;
            }
            if(event.key === Qt.Key_Plus) {
                player1.stopTrace();
                player1.twidth *= 2;
                player1.startTrace();
                event.accepted = true;
            }
            if(event.key === Qt.Key_Minus) {
                player1.stopTrace();
                player1.twidth /= 2;
                player1.startTrace();
                event.accepted = true;
            }
        }
        Keys.onReleased: {
            if(event.isAutoRepeat) return;
            // TODO: loop
            player1.handleKey(event.key, false);
            player2.handleKey(event.key, false);
        }
    }

//    Player {
//        id: player1
//        keyLeft: Qt.Key_Left
//        keyRight: Qt.Key_Right
//        color: "#697614"
//    }

//    Player {
//        id: player2
//        keyLeft: Qt.Key_Q
//        keyRight: Qt.Key_D
//        color: "#FDD041"
//    }

    Rectangle {
        id: sceneRect
        color: "#111"
        anchors.fill: parent

        LayerCanvas {
            id: canvasBg
            anchors.fill: parent
            onAvailableChanged: {
                init();
                control.init();
            }
        }
        LayerCanvas {
            id: canvasFg
            anchors.fill: parent
            onAvailableChanged: {
                init();
                control.init();
            }
        }
        LayerCanvas {
            id: canvasDebug
            anchors.fill: parent
            onAvailableChanged: {
                init();
                control.init();
            }
        }

    }
}
