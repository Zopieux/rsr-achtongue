#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQuick>
#include "engine.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    Engine gameEngine;

    QObject *root = engine.rootObjects().first();
    QQuickItem *control = root->findChild<QQuickItem*>(QStringLiteral("control"));
    Q_ASSERT(control != NULL);
    QObject::connect(control, SIGNAL(readyToInit()), &gameEngine, SLOT(readyToInit()));

    QQmlComponent component(&engine, QUrl(QStringLiteral("qrc:/Player.qml")));

    for(int i = 0; i < 3; i++) {
        QQuickItem *player = qobject_cast<QQuickItem*>(component.create());
        player->setParentItem(control);
        qDebug() << "added player" << player;
        QMetaObject::invokeMethod(player, "init", Q_ARG(QVariant, 20),Q_ARG(QVariant, 30), Q_ARG(QVariant, 0), Q_ARG(QVariant, 30));
    }

    return app.exec();
}
