import QtQuick 2.0

Canvas {
    function init() {
        var ctx = ctx();
        ctx.lineCap = "butt";
        ctx.lineJoin = "bevel";
    }

    function ctx() {
        return getContext("2d");
    }

    function clear() {
        ctx().clearRect(0, 0, width, height);
    }
}
