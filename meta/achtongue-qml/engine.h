#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>

class Engine : public QObject
{
    Q_OBJECT
public:
    explicit Engine(QObject *parent = 0);

signals:

public slots:
    void readyToInit();

};

Q_DECLARE_METATYPE(Engine*)

#endif // ENGINE_H
