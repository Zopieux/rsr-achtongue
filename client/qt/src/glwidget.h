#include <QtOpenGL>
#include <QTime>
#include "player.h"

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent=0);
    ~GLWidget();
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void paintEvent(QPaintEvent *);
    void timerEvent(QTimerEvent *) { update(); }
    bool event(QEvent *);
    void mousePressEvent(QMouseEvent *) { killTimer(timerId); }
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *) { timerId = startTimer(20); }

private:
    int timerId;
    long frameId;
    long jumpCounter;
    qreal zoom;

    QTime absTime;

    QPoint *mousePosition;
    QGLFramebufferObject *fbo;
    QTimer *gameUpdateTimer;

    Player *player;

    void paintLine(double, double, double, double, float);

private slots:
    void gameUpdate();

};
