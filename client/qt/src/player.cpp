#include "player.h"
#include <QtMath>
#include <QDebug>

//#define PAINT_DEBUG

Player::Player(QColor color, QPointF pos, qreal dir, QObject *parent) :
    QObject(parent), stateLock(QMutex::Recursive), _direction(dir), _speed(4), _width(10.f), _turnRadius(40.f), _tracing(true), _inker(0), _move(STRAIGHT), headPos(pos), tailParts(), colors()
{
    // rasta colors
    colors.append(QColor(105, 118, 20));
    colors.append(QColor(253, 208, 65));
    colors.append(QColor(200, 50, 49));

    colorsCycle = colors.begin();
    this->_color = *colorsCycle;

    TailSpec spec = {this->_color, _width, QPointF(headPos), QPointF(), TailSpec::LINE};
    tailParts.append(spec);
}

Player::~Player()
{
    tailParts.clear();
}

void Player::setDirection(qreal other)
{
    _direction = other;
}

qreal Player::direction()
{
    return _direction;
}
qreal Player::width()
{
    return _width;
}
qreal Player::turnRadius()
{
    return _turnRadius;
}
qreal Player::speed()
{
    return _speed;
}

void Player::stopTrace()
{
    _tracing = false;
//    if (!tailParts.isEmpty())
//        tailLastPart().end = QPointF(headPos);
}

void Player::startTrace()
{
    if (_tracing)
        return;
    stateLock.lock();
    TailSpec spec = {_color, _width, QPointF(headPos), QPointF((headPos))};
    if (_move == STRAIGHT) {
        spec.type = TailSpec::LINE;
    } else {
        qreal r = _turnRadius * (_move == RIGHT ? 1.f : -1.f);
        QPointF center(headPos + QPointF(-r * qSin(_direction), r * qCos(_direction)));
        spec.end = QPointF();
        spec.type = TailSpec::CURVE;
        spec.curve = {center, r, _direction};
    }
    stateLock.unlock();
    tailParts.append(spec);
    _tracing = true;
}

static void draw_line( double x1, double y1, double x2, double y2, //coordinates of the line
           float w,                        //width/thickness of the line in pixel
           float Cr, float Cg, float Cb,   //RGB color components
           float Br, float Bg, float Bb,   //color of background when alphablend=false,
           //  Br=alpha of color when alphablend=true
           bool alphablend)                //use alpha blend or not
{
    double t; double R; double f=w-static_cast<int>(w);
    float A;

    if ( alphablend)
        A=Br;
    else
        A=1.0f;

    //determine parameters t,R
    /*   */if ( w>=0.0 && w<1.0) {
        t=0.05; R=0.48+0.32*f;
        if ( !alphablend) {
            Cr+=0.88*(1-f);
            Cg+=0.88*(1-f);
            Cb+=0.88*(1-f);
            if ( Cr>1.0) Cr=1.0;
            if ( Cg>1.0) Cg=1.0;
            if ( Cb>1.0) Cb=1.0;
        } else {
            A*=f;
        }
    } else if ( w>=1.0 && w<2.0) {
        t=0.05+f*0.33; R=0.768+0.312*f;
    } else if ( w>=2.0 && w<3.0){
        t=0.38+f*0.58; R=1.08;
    } else if ( w>=3.0 && w<4.0){
        t=0.96+f*0.48; R=1.08;
    } else if ( w>=4.0 && w<5.0){
        t=1.44+f*0.46; R=1.08;
    } else if ( w>=5.0 && w<6.0){
        t=1.9+f*0.6; R=1.08;
    } else if ( w>=6.0){
        double ff=w-6.0;
        t=2.5+ff*0.50; R=1.08;
    }
    //printf( "w=%f, f=%f, C=%.4f\n", w,f,C);

    //determine angle of the line to horizontal
    double tx=0,ty=0; //core thinkness of a line
    double Rx=0,Ry=0; //fading edge of a line
    double cx=0,cy=0; //cap of a line
    double ALW=0.01;
    double dx=x2-x1;
    double dy=y2-y1;
    if ( qAbs(dx) < ALW) {
        //vertical
        tx=t; ty=0;
        Rx=R; Ry=0;
        if ( w>0.0 && w<=1.0) {
            tx = 0.5; Rx=0.0;
        }
    } else if ( qAbs(dy) < ALW) {
        //horizontal
        tx=0; ty=t;
        Rx=0; Ry=R;
        if ( w>0.0 && w<=1.0) {
            ty = 0.5; Ry=0.0;
        }
    } else {
        if ( w < 3) { //approximate to make things even faster
            double m=dy/dx;
            //and calculate tx,ty,Rx,Ry
            if ( m>-0.4142 && m<=0.4142) {
                // -22.5< angle <= 22.5, approximate to 0 (degree)
                tx=t*0.1; ty=t;
                Rx=R*0.6; Ry=R;
            } else if ( m>0.4142 && m<=2.4142) {
                // 22.5< angle <= 67.5, approximate to 45 (degree)
                tx=t*-0.7071; ty=t*0.7071;
                Rx=R*-0.7071; Ry=R*0.7071;
            } else if ( m>2.4142 || m<=-2.4142) {
                // 67.5 < angle <=112.5, approximate to 90 (degree)
                tx=t; ty=t*0.1;
                Rx=R; Ry=R*0.6;
            } else if ( m>-2.4142 && m<-0.4142) {
                // 112.5 < angle < 157.5, approximate to 135 (degree)
                tx=t*0.7071; ty=t*0.7071;
                Rx=R*0.7071; Ry=R*0.7071;
            } else {
                // error in determining angle
                //printf( "error in determining angle: m=%.4f\n",m);
            }
        } else { //calculate to exact
            dx=y1-y2;
            dy=x2-x1;
            double L=sqrt(dx*dx+dy*dy);
            dx/=L;
            dy/=L;
            cx=-dy; cy=dx;
            tx=t*dx; ty=t*dy;
            Rx=R*dx; Ry=R*dy;
        }
    }

    x1+=cx*0.5; y1+=cy*0.5;
    x2-=cx*0.5; y2-=cy*0.5;

    //draw the line by triangle strip
    float line_vertex[]=
    {
        x1-tx-Rx-cx, y1-ty-Ry-cy, //fading edge1
        x2-tx-Rx+cx, y2-ty-Ry+cy,
        x1-tx-cx,y1-ty-cy,        //core
        x2-tx+cx,y2-ty+cy,
        x1+tx-cx,y1+ty-cy,
        x2+tx+cx,y2+ty+cy,
        x1+tx+Rx-cx, y1+ty+Ry-cy, //fading edge2
        x2+tx+Rx+cx, y2+ty+Ry+cy
    };

    glVertexPointer(2, GL_FLOAT, 0, line_vertex);

    if ( !alphablend) {
        float line_color[]=
        {
            Br,Bg,Bb,
            Br,Bg,Bb,
            Cr,Cg,Cb,
            Cr,Cg,Cb,
            Cr,Cg,Cb,
            Cr,Cg,Cb,
            Br,Bg,Bb,
            Br,Bg,Bb
        };
        glColorPointer(3, GL_FLOAT, 0, line_color);
    } else {
        float line_color[]=
        {
            Cr,Cg,Cb,0,
            Cr,Cg,Cb,0,
            Cr,Cg,Cb,A,
            Cr,Cg,Cb,A,
            Cr,Cg,Cb,A,
            Cr,Cg,Cb,A,
            Cr,Cg,Cb,0,
            Cr,Cg,Cb,0
        };
        glColorPointer(4, GL_FLOAT, 0, line_color);
    }

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 8);

    //cap
    if ( w < 3) {
        //do not draw cap
    } else {
        //draw cap
        float line_vertex[]=
        {
            x1-tx-Rx-cx, y1-ty-Ry-cy, //cap1
            x1-tx-Rx, y1-ty-Ry,
            x1-tx-cx, y1-ty-cy,
            x1+tx+Rx, y1+ty+Ry,
            x1+tx-cx, y1+ty-cy,
            x1+tx+Rx-cx, y1+ty+Ry-cy,
            x2-tx-Rx+cx, y2-ty-Ry+cy, //cap2
            x2-tx-Rx, y2-ty-Ry,
            x2-tx+cx, y2-ty+cy,
            x2+tx+Rx, y2+ty+Ry,
            x2+tx+cx, y2+ty+cy,
            x2+tx+Rx+cx, y2+ty+Ry+cy
        };

        glVertexPointer(2, GL_FLOAT, 0, line_vertex);

        if ( !alphablend) {
            float line_color[]=
            {
                Br,Bg,Bb, //cap1
                Br,Bg,Bb,
                Cr,Cg,Cb,
                Br,Bg,Bb,
                Cr,Cg,Cb,
                Br,Bg,Bb,
                Br,Bg,Bb, //cap2
                Br,Bg,Bb,
                Cr,Cg,Cb,
                Br,Bg,Bb,
                Cr,Cg,Cb,
                Br,Bg,Bb
            };
            glColorPointer(3, GL_FLOAT, 0, line_color);
        } else {
            float line_color[]=
            {
                Cr,Cg,Cb, 0, //cap1
                Cr,Cg,Cb, 0,
                Cr,Cg,Cb, A,
                Cr,Cg,Cb, 0,
                Cr,Cg,Cb, A,
                Cr,Cg,Cb, 0,
                Cr,Cg,Cb, 0, //cap2
                Cr,Cg,Cb, 0,
                Cr,Cg,Cb, A,
                Cr,Cg,Cb, 0,
                Cr,Cg,Cb, A,
                Cr,Cg,Cb, 0
            };
            glColorPointer(4, GL_FLOAT, 0, line_color);
        }

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 6);
        glDrawArrays(GL_TRIANGLE_STRIP, 6, 6);
    }
}

//void Player::drawCircleArc(QPainter &painter, Player::TailSpec &spec)
//{
//    QPointF &center = spec.curve.center;
//    qreal r = spec.curve.radius;
//    QRectF rect(center.x() - r, center.y() - r, r * 2.f, r * 2.f);
//    QPointF &start = spec.start,
//            &end = spec.end;
//    QLineF line(start, center);
//    qreal angle = line.angle() - 180.f;
//    qreal sweep = line.angleTo(QLineF(end, center));
//    if (r > 0)
//        sweep = -(360.f - sweep);
//    if (sweep == -360) {
//        // bugfix
//        sweep = 0.f;
//    }
//    painter.drawArc(rect, angle * 16.f, sweep * 16.f);
//#ifdef PAINT_DEBUG
//    painter.save();
////    painter.drawLine(spec.start, spec.end);
////    painter.setPen(Qt::white);
////    painter.setBrush(Qt::NoBrush);
//    painter.setPen(QPen(Qt::white, 1));
////    painter.drawEllipse(rect);
////    painter.drawPoint(spec.start);
////    painter.drawPoint(spec.end);
//    painter.drawRect(rect);
//    painter.drawPoint(center);
//    painter.restore();
//#endif
//}

void Player::drawCircleArc(TailSpec &spec)
{
    QPointF p(spec.start);
    QPointF p2;
    qreal dir = spec.curve.dir;
    qreal s = _speed * 0.3f;
    int fuck = 0;
    do {
        dir += (spec.curve.radius > 0 ? 1 : -1) * 2.f * qAsin(.5f * s / _turnRadius);
        p2 = p + (QPointF(qCos(dir), qSin(dir)) * s);
        draw_line(p.x(), p.y(),
                  p2.x(), p2.y(),
                  spec.width,
                  spec.color.redF(), spec.color.greenF(), spec.color.blueF(), 1.f,
                  0, 0, true);
        qSwap(p, p2);
    } while(fuck++ < 1000 && QLineF(p2, spec.end).length() > 2.f);
}

static void draw_circle(float x, float y, float radius) {
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(x, y, 0.0f);
    static const int circle_points = 30;
    static const float angle = M_PI * 2.f / circle_points;

    // this code (mostly) copied from question:
    glBegin(GL_POLYGON);
    double angle1=0.0;
    glVertex2d(radius * qCos(0.0) , radius * qSin(0.0));
    int i;
    for (i=0; i<circle_points; i++)
    {
        glVertex2d(radius * qCos(angle1), radius * qSin(angle1));
        angle1 += angle;
    }
    glEnd();
    glPopMatrix();
}

void Player::paint(QGLWidget &qgl)
{
    // head
//    painter.save();
//    painter.setPen(Qt::NoPen);
//    painter.setBrush(_color);
//    painter.drawEllipse(headPos, _width/2.f, _width/2.f);
//    painter.restore();

//    painter.save();

    glPointSize(_width);
    qgl.qglColor(_color);
    draw_circle(headPos.x(), headPos.y(), _width / 2.f);

    // tail parts
    QLinkedList<TailSpec>::iterator part;

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    for (part = tailParts.begin(); part != tailParts.end(); ++part)
    {
        TailSpec &spec = *part;
        qgl.qglColor(spec.color);
//        painter.setPen(QPen((*part).color, (*part).width, Qt::SolidLine, Qt::FlatCap));
        if (spec.type == TailSpec::LINE) {
            draw_line(spec.start.x(), spec.start.y(),
                      spec.end.x(), spec.end.y(),
                      spec.width,
                      spec.color.redF(), spec.color.greenF(), spec.color.blueF(), 1.f,
                      0, 0, true);
//            painter.drawLine(spec.start, spec.end);
//#ifdef PAINT_DEBUG
//            painter.save();
//            painter.setPen(QPen(Qt::white, 2));
//            painter.drawPoint(spec.start);
//            painter.drawPoint(spec.end);
//            painter.restore();
//#endif
        } else if (spec.type == TailSpec::CURVE) {
            if (!spec.end.isNull()) {
//                drawCircleArc(painter, *part);
                drawCircleArc(spec);
            }
        }
    }
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
//    painter.restore();
}

void Player::step()
{
    stateLock.lock();
    if (_move != STRAIGHT)
        _direction += (_move == RIGHT ? 1 : -1) * 2.f * qAsin(.5f * _speed / _turnRadius);

    headPos += QPointF(_speed * qCos(_direction), _speed * qSin(_direction));

    if (tracing())
        tailLastPart().end = QPointF(headPos);
    stateLock.unlock();

    // color switcher
    _inker++;
    if (_inker > 5 * _width) {
        if((++colorsCycle) == colors.end())
            colorsCycle = colors.begin();
        _color = *colorsCycle;
        resetTrace();
        _inker = 0;
    }
}

Player::TailSpec& Player::tailLastPart()
{
    return tailParts.last();
}

bool Player::tracing()
{
    return _tracing;
}

void Player::goStraight()
{
    if (_move == STRAIGHT)
        return;
    _move = STRAIGHT;
    resetTrace();
}

void Player::turnLeft()
{
    if (_move == LEFT)
        return;
    _move = LEFT;
    resetTrace();
}

void Player::turnRight()
{
    if (_move == RIGHT)
        return;
    _move = RIGHT;
    resetTrace();
}

void Player::clearTail()
{
    tailParts.clear();
    resetTrace();
}

void Player::setSpeed(qreal speed) {
    this->_speed = speed;
    resetTrace();
}

void Player::setTurnRadius(qreal radius) {
    this->_turnRadius = radius;
    resetTrace();
}

void Player::setWidth(qreal width) {
    this->_width = width;
    resetTrace();
}

void Player::resetTrace() {
    stateLock.lock();
    if (tracing()) {
        stopTrace();
        startTrace();
    }
    stateLock.unlock();
}
