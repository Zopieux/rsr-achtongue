#-------------------------------------------------
#
# Project created by QtCreator 2014-10-25T14:26:27
#
#-------------------------------------------------

QT       += opengl core gui
#QMAKE_CXXFLAGS += -DPAINT_DEBUG

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = achtongue
TEMPLATE = app


SOURCES += main.cpp\
    glwidget.cpp \
    player.cpp

HEADERS  += \
    glwidget.h \
    player.h

FORMS    += mainwindow.ui
