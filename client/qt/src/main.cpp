//#include "mainwindow.h"
#include <QApplication>
#include <QtWidgets/QMessageBox>
#include "glwidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (!QGLFormat::hasOpenGL() || !QGLFramebufferObject::hasOpenGLFramebufferObjects()) {
        QMessageBox::information(0, "OpenGL framebuffer objects 2",
                                 "This system does not support OpenGL/framebuffer objects.");
        return -1;
    }

    qsrand(QTime::currentTime().msec());

    GLWidget widget(0);
    widget.resize(640, 480);
    widget.show();

    return a.exec();
}
