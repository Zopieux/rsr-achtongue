#include "glwidget.h"
#include <QtGui/QImage>

#include <math.h>


GLWidget::GLWidget(QWidget *parent)
  : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    // create the framebuffer object - make sure to have a current
    // context before creating it
    makeCurrent();
    mousePosition = new QPoint(0, 0);
    fbo = new QGLFramebufferObject(512, 512, QGLFramebufferObject::Depth);
    timerId = startTimer(20);
    frameId = 0;
    jumpCounter = 0;
    zoom = 1;

    QTimer* gameUpdateTimer = new QTimer();
    connect(gameUpdateTimer, SIGNAL(timeout()), this, SLOT(gameUpdate()));
    gameUpdateTimer->start(20);

    player = new Player(Qt::red, QPointF(512/2, 512/2), 0, this);
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    *mousePosition = event->pos();
}

GLWidget::~GLWidget()
{
    delete fbo;
}

void GLWidget::initializeGL()
{
    glMatrixMode(GL_MODELVIEW);
    glShadeModel(GL_SMOOTH);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
//    glEnable(GL_DEPTH_TEST);
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClearDepth(1.f);
}

void GLWidget::resizeGL(int w, int h)
{
    qDebug() << "resizeGL" << w << h;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, w, h, 0, -6, 6);
    glScalef(1, 1, 1);
//    float aspect = w/(float)(h ? h : 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GLWidget::paintLine(double x1, double y1, double x2, double y2, float thickness)
{
    qreal slope = qAtan2(y2 - y1, x2 - x1);
    thickness /= 2.f;
    glBegin(GL_POLYGON);
    glVertex2d(x1 + thickness * qSin(slope), y1 - thickness * qCos(slope));
    glVertex2d(x2 + thickness * qSin(slope), y2 - thickness * qCos(slope));
    glVertex2d(x2 - thickness * qSin(slope), y2 + thickness * qCos(slope));
    glVertex2d(x1 - thickness * qSin(slope), y1 + thickness * qCos(slope));
    glEnd();
}

bool GLWidget::event(QEvent *event)
{
    if (event->type() == QEvent::Wheel) {
        QWheelEvent *wheelEvent = static_cast<QWheelEvent*>(event);
        int delta = wheelEvent->delta();
        if (delta > 0)
            zoom *= 1.5f;
        else
            zoom /= 1.5f;
        qDebug() << "zoom" << zoom;
        player->setSpeed(zoom * 2);
        return true;
    }
    if (event->type() == QEvent::KeyPress || event->type() == QEvent::KeyRelease) {
        bool released = event->type() == QEvent::KeyRelease;
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
        if (keyEvent->isAutoRepeat())
            goto fuck;
        int key = keyEvent->key();
        if (released) {
            if (key == Qt::Key_Left || key == Qt::Key_Right) {
                player->goStraight();
            }
            if (key == Qt::Key_Space)
                player->startTrace();
        } else if (key == Qt::Key_Left) {
            player->turnLeft();
            return true;
        } else if (key == Qt::Key_Right) {
            player->turnRight();
            return true;
        } else if (key == Qt::Key_Space) {
            player->stopTrace();
        } else if (key == Qt::Key_C) {
            player->clearTail();
        }
    }
    fuck:
    return QWidget::event(event);
}

void GLWidget::paintEvent(QPaintEvent *event)
{
    if (!(frameId % 100)) {
        setWindowTitle(QString("Nique — %1").arg(frameId / (absTime.elapsed() / 1000.0)));
        absTime.start();
        frameId = 0;
    }
    frameId++;
    QGLWidget::paintEvent(event);
//    p.beginNativePainting();
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    glClearColor(0.f, 0.f, 0.f, 0.f);
//    glLoadIdentity();
//    player->paint(*_painter);
//    _painter->end();
//    p.endNativePainting();
//    p.end();
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    player->paint(*this);
}

void GLWidget::gameUpdate()
{
    player->step();
}
