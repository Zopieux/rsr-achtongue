#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QPointF>
#include <QColor>
#include <QMutex>
#include <QLinkedList>
#include <QPainter>
#include <QtOpenGL>


class Player : public QObject
{
    Q_OBJECT
public:
    explicit Player(QColor _color, QPointF pos, qreal _direction, QObject *parent = 0);
    ~Player();

    bool tracing();
    qreal direction();
    qreal speed();
    qreal width();
    qreal turnRadius();

    void setDirection(qreal);
    void setTurnRadius(qreal);
    void setWidth(qreal);
    void setSpeed(qreal);

    void stopTrace();
    void startTrace();
    void clearTail();

    void goStraight();
    void turnLeft();
    void turnRight();

    void paint(QGLWidget &qgl);
    void step();


private:
    QMutex stateLock;

    QColor _color;
    qreal _direction;
    qreal _speed;
    qreal _width;
    qreal _turnRadius;
    bool _tracing;
    unsigned _inker;
    enum Move { STRAIGHT, LEFT, RIGHT } _move;

    QPointF headPos;

    struct TailSpec {
        QColor color;
        qreal width;
        QPointF start, end;
        enum Type { LINE, CURVE } type;
        struct Curve {
            QPointF center;
            qreal radius;
            qreal dir;
        } curve;
    };
    QLinkedList<TailSpec> tailParts;
    QList<QColor> colors;
    QList<QColor>::iterator colorsCycle;

    void resetTrace();
    TailSpec& tailLastPart();
    void drawCircleArc(TailSpec &spec);


signals:

public slots:

};

#endif // PLAYER_H
