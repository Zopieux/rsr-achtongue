#ifndef GAMESERVER_H
#define GAMESERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
#include "../meta/achtongue-graphicscene/engine/protocol.h"
#include <gameroom.h>

class gameServer : public QObject
{
    Q_OBJECT
    protocol prot;
public:
    explicit gameServer(QObject *parent = 0);


public slots:
    void newConnection();

private:
    /* replace this by a list of room for a multiroom server*/
    GameRoom r;
    QTcpServer *server;
    QVector<QColor> officialColors;
};

#endif // GAMESERVER_H
