#include "playerthread.h"

PlayerThread::PlayerThread(int socketDescriptor, QObject* parentRoom, QObject *parent) :
    QObject(parent), socketDescriptor(socketDescriptor), room(parentRoom)
{

    emit initIsFull();
    qDebug() << "playerThread is ready";
    //this->start();
    //run();
}


void PlayerThread::run()
{
    qDebug() << "PlayerThread::run::currentThread "<<QThread::currentThread();
    qDebug("PlayerThread:: a player is trying to join the room");
    socket = new QTcpSocket();
    connect(socket,SIGNAL(readyRead()), this, SLOT(readData()));
    //qRegisterMetaType(socket);
     if (!socket->setSocketDescriptor(socketDescriptor)) {
            qDebug() << "problem while trying to join player";
            emit error(socket->error());
            return;
          }
     qDebug() << "PlayerThread:: socket is Fine";
     // check if there is enough room in the game
     /*
     qDebug() << "PlayerThread:: still some space in the room";
        // read joinRoom
        socket->waitForReadyRead(3000);
        QByteArray data = socket->readAll();
        if(protocol::isJoinRoom(data))
        {
            int id = protocol::getIdFromJoinRoom(data);
            //emit playerIdRetreived(id);
            playerId = id;
            qDebug() << "the player is connected with the id " << id;
            emit newPlayerConnected();
            while(socket->waitForReadyRead())
            {
                mutex.lock();
                QByteArray action = socket->readAll();
                mutex.unlock();
                if (protocol::isAction(action))
                {
                    emit newAction(action);
                }
            }
        }

     socket->disconnectFromHost();
     socket->waitForDisconnected();
     */
}

void PlayerThread::readData()
{
    qDebug() << "PlayerThread::readData::currentThread "<<QThread::currentThread();
    mutex.lock();
    QByteArray data = socket->readAll();
    mutex.unlock();
    qDebug() << "PlayerThread::readData::socket access::currentThread "<<QThread::currentThread();
    if (protocol::isAction(data))
    {
        emit newAction(data);
    }
    else if(protocol::isJoinRoom(data))
    {
        int id = protocol::getIdFromJoinRoom(data);
        //emit playerIdRetreived(id);
        playerId = id;
        qDebug() << "the player is connected with the id " << id;
        qDebug()<<"PlayerThread::readData::emit newPlayerConnected::currentThread "<<QThread::currentThread();
        emit newPlayerConnected();

    }
}

void PlayerThread::notificationToSend(QByteArray packet)
{
    qDebug()<<"PlayerThread::notificationToSend::currentThread "<<QThread::currentThread();
    mutex.lock();
    socket->write(packet);
    socket->waitForBytesWritten(1000);
    mutex.unlock();
}


void PlayerThread::setIsFull(bool ok)
{
    qDebug() << "PlayerThread::setIsFull";
    mutex.lock();
    isFull = ok;
    mutex.unlock();
}

void PlayerThread::gameStating()
{
    qDebug()<< "PlayerThread::gameStarting::currentThread "<<QThread::currentThread();
    static const int m = 40;
    GameRoom* r = room;
    qreal direction = (qrand() % 1000) * M_PI * 2 / 1000;
    // not using QPointF maybe it needs to change?
    int xPos = qrandRange(m,r->getWidth() - m);
    int yPos = qrandRange(m,r->getHeight() - m);
    mutex.lock();
    qDebug() << "PlayerThread:: readyToWriteOnSocket::currentThread "<<QThread::currentThread();
    socket->write(protocol::gameStart(xPos,yPos,direction));
    qDebug() << "PlayerThread:: data written::currentThread "<<QThread::currentThread();
    socket->waitForBytesWritten(3000);
    int id = playerId;
    mutex.unlock();

    qDebug()<<"PlayerThread:: startGame sent to "<<id;
}

static int PlayerThread::qrandRange(int low, int high)
{
    return qrand() % ((high + 1) - low) + low;
}

void PlayerThread::setPlayerId(int id)
{
    qDebug()<<"PlayerThread::setPlayerId::currentThread "<<QThread::currentThread();
    mutex.lock();
    playerId = id;
    mutex.unlock();

}
