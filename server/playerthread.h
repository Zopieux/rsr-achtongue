#ifndef PLAYERTHREAD_H
#define PLAYERTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QByteArray>
#include <QMutex>
#include <gameroom.h>
#include <../meta/achtongue-graphicscene/engine/protocol.h>

class PlayerThread: public QObject
{
    Q_OBJECT
public:
    explicit PlayerThread(int socketDescriptor, QObject* parentRoom, QObject* parent=0);

private:
    static int qrandRange(int low, int high);

signals:
    void error(QTcpSocket::SocketError socketError);
    void newAction(QByteArray action);
    void newPlayerConnected();
    void initIsFull();
    void readFromSocket(QByteArray);
    void playerIdRetreived(int);

public slots:
    void notificationToSend(QByteArray packet);
    void setIsFull(bool);
    void gameStating();
    void setPlayerId(int);
    void readData();
    void run();



private:
    int socketDescriptor;
    QObject* room;
    QTcpSocket * socket;
    QMutex mutex;
    int playerId;
    bool isFull;
};

#endif // PLAYERTHREAD_H
