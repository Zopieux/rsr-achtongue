#ifndef GAMEMATRIX_H
#define GAMEMATRIX_H

class GameMatrix
{
private:
    int nb_lines, nb_cols;
    int** matrix;
public:
    GameMatrix();
    void initialize();
    int get_nb_lines();
    int get_nb_cols();
};

#endif // GAMEMATRIX_H
