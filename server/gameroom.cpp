#include <gameroom.h>
GameRoom::GameRoom(QObject *parent)
     : QTcpServer(parent)
{
    //connect(this,SIGNAL(newConnection()),this,SLOT(newConnection()));

    if(!this->listen(QHostAddress::Any, port))
    {
        qDebug() << "Room Server could not start";
    }
    else
    {
        qDebug() << "Room Server started!";
    }
}


void GameRoom::incomingConnection(qintptr socketDescriptor)
{
     qDebug() << "GameRoom::incominConnection::currentThread "<<QThread::currentThread();
     QThread * t = new QThread(this->thread());
     PlayerThread *client = new PlayerThread(socketDescriptor,this);
     //connect(client, SIGNAL(finished()), client, SLOT(deleteLater()));
     connect(client, SIGNAL(newAction(QByteArray)),this, SLOT(actionReceived(QByteArray)));
     connect(this, SIGNAL(notifyPlayers(QByteArray)),client, SLOT(notificationToSend(QByteArray)));
     connect(client,SIGNAL(newPlayerConnected()),this,SLOT(newPlayer()));
     connect(client,SIGNAL(initIsFull()),this,SLOT(initIsFull()));
     connect(this,SIGNAL(setIsFull(bool)),client,SLOT(setIsFull(bool)));
     connect(this,SIGNAL(startGame()),client,SLOT(gameStating()));
     connect(t,SIGNAL(started()),client,SLOT(run()));
     client->moveToThread(t);
     //thread->start();
     t->start();
}

void GameRoom::newPlayer()
{
    qDebug() <<"GameRoom::newPlayer";
    mutex.lock();
    connectedPlayers ++;
    mutex.unlock();
    if(isFull())
    {
        qDebug() <<"GameRoom::newPlayer::emit startGame::currentThread "<<QThread::currentThread();
        emit startGame();
    }
}

void GameRoom::actionReceived(QByteArray action)
{
    qDebug() <<"GameRoom::actionReceived";
    emit notifyPlayers(action);
}

bool GameRoom::isFull()
{
    bool ok =  true;
    mutex.lock();
    if (connectedPlayers < capacity)
    {
        ok = false;
    }
    mutex.unlock();
    return ok;
}

int GameRoom::getId()
{
    int n;
    mutex.lock();
    n = authorizedPlayers;
    mutex.unlock();
    initIsFull();
    if (n < capacity)
    {
        return n;
    }
    else
    {
        return -1;
    }

}

int GameRoom::getPort()
{
    int n;
    mutex.lock();
    n = port;
    mutex.unlock();
    return n;
}

void GameRoom::addPlayer(player p)
{
    qDebug() <<"GameRoom::addPlayer";
    mutex.lock();
    if( authorizedPlayers < capacity && connectedPlayers < capacity)
    {
        players.append(p);
        authorizedPlayers ++;
    }
    mutex.unlock();
}
void GameRoom::newConnection()
{
    qDebug() << "gameRoom::newConnection ";

}

void GameRoom::initIsFull()
{
    bool ok = true;
    mutex.lock();
    if (authorizedPlayers < capacity)
        ok = false;
    mutex.unlock();

    emit setIsFull(ok);
}

int GameRoom::getHeight()
{
    int n;
    mutex.lock();
    n = height;
    mutex.unlock();
    return n;
}


int GameRoom::getWidth()
{
    int n;
    mutex.lock();
    n = width;
    mutex.unlock();
    return n;
}
