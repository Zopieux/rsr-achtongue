#-------------------------------------------------
#
# Project created by QtCreator 2015-01-09T21:25:45
#
#-------------------------------------------------

QT       += core
QT       += network

QT       += gui

TARGET = server
CONFIG   += console
CONFIG   -= app_bundle
QMAKE_CXXFLAGS += -std=c++0x -pthread -fpermissive
LIBS += -pthread

TEMPLATE = app


SOURCES += main.cpp \
    gameserver.cpp \
    player.cpp \
    ../meta/achtongue-graphicscene/engine/protocol.cpp \
    gameroom.cpp \
    playerthread.cpp
HEADERS += \
    gameserver.h \
    player.h \
    ../meta/achtongue-graphicscene/engine/protocol.h \
    gameroom.h \
    playerthread.h
