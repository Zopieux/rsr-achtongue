#include "gameserver.h"

gameServer::gameServer(QObject *parent) :
    QObject(parent)
{
    server = new QTcpServer(this);

    // whenever a user connects, it will emit signal
    connect(server, SIGNAL(newConnection()),
            this, SLOT(newConnection()));
    officialColors << QColor(255, 233, 43) << QColor(2, 209, 192) << QColor(255, 164, 186)
                   << QColor(68, 68, 255) << QColor(106, 255, 48) << QColor(255, 136, 52)
                   << QColor(255, 69, 69) << QColor(255, 255, 255);
    if(!server->listen(QHostAddress::Any, 12345))
    {
        qDebug() << "Server could not start";
    }
    else
    {
        qDebug() << "Server started!";
    }
}

void gameServer::newConnection()
{
    // need to grab the socket
    QTcpSocket *socket = server->nextPendingConnection();
    socket->waitForReadyRead(3000);
    qDebug() << "Reading: " << socket->bytesAvailable();
    QByteArray data = socket->readAll();
    qDebug() << data;
    int id = r.getId();
    if( id >= 0)
    {
        player p(protocol::getUsernameFromHelloServer(data),id);
        r.addPlayer(p);

        socket->write(protocol::gameParams(id,r.getPort(), 100, 100, officialColors.at(0)));
        socket->waitForBytesWritten(3000);

    }
    else
    {
        socket->write(protocol::roomFull());
        socket->waitForBytesWritten(3000);
    }
    socket->close();
}
