#ifndef PLAYER_H
#define PLAYER_H
#include <QHostAddress>
#include <QString>
class player
{
    QString username;
    int id;
public:
    player(QString,int);
    QString getUserName();
    int getId();
};

#endif // PLAYER_H
