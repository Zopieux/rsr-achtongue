#ifndef GAMEROOM_H
#define GAMEROOM_H
#include <QTcpServer>
#include <QMutex>
#include <QByteArray>
#include <player.h>
#include <QList>
#include <QDebug>
#include <playerthread.h>

class GameRoom : public QTcpServer
 {
     Q_OBJECT

 public:
     GameRoom(QObject *parent = 0);
     bool isFull();
     int getId();
     int getPort();
     void addPlayer(player);
     int getWidth();
     int getHeight();
 protected:
     void incomingConnection(qintptr socketDescriptor);

signals:
    void notifyPlayers(QByteArray packet);
    void setIsFull(bool);
    void startGame();

 public slots:
     void actionReceived(QByteArray action);
     void newPlayer();
     void newConnection();
     void initIsFull();

 private:
    QMutex mutex;
    int capacity = 2;
    int connectedPlayers = 0;
    int authorizedPlayers = 0;
    QList<player> players;
    int port = 5000;
    int width = 700;
    int height = 700;

 };
#endif // GAMEROOM_H
