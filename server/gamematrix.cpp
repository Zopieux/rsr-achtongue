#include "gamematrix.h"

GameMatrix::GameMatrix(int lines, int cols):nb_lines(lines),nb_cols(cols)
{
    matrix=new int[lines][cols]
}

GameMatrix::initialize()
{
    int i,j;
    for (i=0;i<lines;i++) {
        for(j=0;j<cols;j++){
            matrix[i][j]=0;
        }
    }
}

GameMatrix::get_nb_lines()
{
    return nb_lines;
}

GameMatrix::get_nb_cols()
{
    return nb_cols;
}
